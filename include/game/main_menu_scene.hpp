#pragma once

#include <psemek/app/scene.hpp>
#include <psemek/app/scene_manager.hpp>

#include <memory>
#include <functional>

using namespace psemek;

std::unique_ptr<app::scene> make_main_menu_scene(std::function<void()> on_quit, app::scene_manager & scene_manager);
