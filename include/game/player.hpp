#pragma once

#include <game/units.hpp>
#include <game/path_finder.hpp>

#include <variant>
#include <optional>
#include <memory>

namespace states
{

	struct start_game
	{
		float cooldown = 1.f;
	};

	struct select{};

	struct move
	{
		units::id selected_unit;
		int moves;
		path_finder::reachable_map reachable_tiles;
		path_finder::tile_set shootable_tiles;
		path_finder::melee_map melee_tiles;
		path_finder::reachable_map choppable_tiles;
		path_finder::reachable_map buildable_tiles;
	};

	struct end_turn
	{
		float cooldown = 0.5f;
	};

	struct victory
	{};

	struct draw
	{};

}

using game_state = std::variant<
	states::start_game,
	states::select,
	states::move,
	states::end_turn,
	states::victory,
	states::draw
>;

namespace actions
{

	struct select
	{
		units::id id;
	};

	struct move
	{
		geom::point<int, 2> tile;
	};

	struct shoot
	{
		geom::point<int, 2> target;
	};

	struct melee
	{
		geom::point<int, 2> position;
		geom::point<int, 2> target;
	};

	struct chop
	{
		geom::point<int, 2> target;
	};

	struct build
	{
		geom::point<int, 2> target;
		tile type;
	};

	struct skip
	{};

}

using player_action = std::variant<
	actions::select,
	actions::move,
	actions::shoot,
	actions::melee,
	actions::chop,
	actions::build,
	actions::skip
>;

struct player_controller
{
	virtual std::string_view name() const = 0;

	virtual void set_game_state(game_state const & state) = 0;

	virtual void on_human_action(player_action const & action) = 0;

	virtual bool free_camera() = 0;
	virtual bool show_ui() = 0;

	virtual std::optional<player_action> action() = 0;

	virtual ~player_controller(){}
};

std::unique_ptr<player_controller> make_human_controller(int player, units const & u);

std::unique_ptr<player_controller> make_offensive_ai_controller(int player, units const & u, path_finder const & pf, random::generator rng);
std::unique_ptr<player_controller> make_defensive_ai_controller(int player, units const & u, path_finder const & pf, random::generator rng);
std::unique_ptr<player_controller> make_dummy_ai_controller();
