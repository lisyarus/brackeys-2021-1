#pragma once

#include <game/map.hpp>

#include <psemek/app/scene.hpp>

#include <memory>
#include <functional>

struct units;
struct path_finder;
struct player_controller;

using player_controller_factory = std::function<std::unique_ptr<player_controller>(units const &, path_finder const & pf)>;

std::unique_ptr<app::scene> make_battle_scene(map m, std::function<void()> on_quit, player_controller_factory player0, player_controller_factory player1);
