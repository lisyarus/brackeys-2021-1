#pragma once

#include <filesystem>

struct settings
{
	bool auto_camera = true;
	bool inverted_y = false;
	bool inverted_wheel = false;

	float mouse_sensitivity = 1.f;
	float sound_volume = 1.f;

	void load(std::filesystem::path path);
	void save(std::filesystem::path path);
};

settings & global_settings();
