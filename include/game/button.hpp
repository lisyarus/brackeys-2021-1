#pragma once

#include <psemek/gfx/painter.hpp>
#include <psemek/gfx/texture.hpp>

#include <psemek/geom/point.hpp>

#include <functional>

using namespace psemek;

struct button
{
	static gfx::color_rgba const default_color;

	button(std::string_view text, int width, int height, gfx::color_rgba color = default_color);
	button(gfx::texture_2d image, int scale, std::string_view helper_text);

	void set_text(std::string_view text) { text_ = text; }

	int width() const { return width_ * scale_; }
	int height() const { return height_ * scale_; }

	void set_position(geom::point<int, 2> const & position);
	geom::point<int, 2> position() const { return position_; }

	void set_on_click(std::function<void()> callback);

	void set_active(bool active);
	bool active() const { return active_; }

	virtual void render(gfx::painter & painter);

	virtual bool on_mouse_move(geom::point<int, 2> const & mouse);
	virtual bool on_mouse_down();
	virtual bool on_mouse_up();

protected:
	std::string text_;
	gfx::texture_2d image_ = gfx::texture_2d::null();
	int width_;
	int height_;
	int scale_ = 1;
	gfx::color_rgba color_;
	geom::point<int, 2> position_{0, 0};
	std::function<void()> callback_;
	bool hover_ = false;
	bool down_ = false;
	bool active_ = true;
};
