#pragma once

#include <game/map.hpp>
#include <game/render.hpp>

#include <psemek/gfx/program.hpp>

#include <psemek/geom/point.hpp>

#include <optional>
#include <array>

struct ramps
{
	ramps(map const & m);

	std::optional<tile> ramp_at(int x, int y) const;
	bool has_ramp(int x, int y) const;
	void add_ramp(geom::point<int, 2> const & t, tile type);
	void add_flat_ramp(int x, int y);
	void add_slope_ramp(int x, int y, int direction);

	std::optional<std::array<geom::point<float, 3>, 4>> ramp_surface(int x, int y) const;

	std::optional<int> effective_height(geom::point<int, 2> const & tile) const;
	std::optional<tile> effective_geometry(geom::point<int, 2> const & tile) const;
	std::optional<std::array<geom::point<float, 3>, 4>> effective_surface(geom::point<int, 2> const & tile) const;
	std::optional<float> effective_center_height(geom::point<int, 2> const & tile) const;

	float height_at(float x, float y) const;
	geom::vector<float, 3> normal_at(float x, float y) const;

	void update(float dt);
	bool has_animation() const;

	void render(render_options const & options);
	void render_ghost(render_options const & options, geom::point<int, 2> const & position, tile type, gfx::color_4f const & color);

private:
	map const & map_;

	random::generator rng_;

	struct ramp_data
	{
		tile type;
		geom::point<float, 3> position{};
		float rotation = 0.f;
		float appearing = 0.f;
	};

	util::array<std::vector<ramp_data>, 2> ramps_;

	int appearing_ramps_ = 0;

	gfx::program program_;
	gfx::mesh flat_mesh_;
	gfx::mesh slope_mesh_;
};
