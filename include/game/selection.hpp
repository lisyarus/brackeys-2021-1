#pragma once

#include <game/map.hpp>
#include <game/render.hpp>

#include <psemek/gfx/program.hpp>

using namespace psemek;

struct selection
{
	selection();

	struct tile
	{
		std::array<geom::point<float, 3>, 4> points;
		float height;
		gfx::color_4f color;
	};

	void render(render_options const & options, std::vector<tile> const & tiles);

private:
	gfx::program program_;
	gfx::array vao_;
	gfx::buffer vbo_;
};
