#pragma once

#include <game/map.hpp>
#include <game/render.hpp>

#include <psemek/gfx/program.hpp>

using namespace psemek;

struct terrain
{
	terrain(map const & m);

	void render(render_options const & options);

private:
	gfx::program program_;
	gfx::array vao_;
	gfx::buffer vbo_;
	gfx::buffer ebo_;
	int count_ = 0;

	gfx::texture_2d_array texture_;

	void generate_texture();
	void generate_buffers(map const & m);
};
