#pragma once

struct sound
{
	static void click();
	static void select();
	static void move();
	static void fire();
	static void arrow_hit();
	static void melee_hit();
	static void chop();
	static void build();
};
