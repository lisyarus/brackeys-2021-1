#pragma once

#include <game/map.hpp>
#include <game/render.hpp>
#include <game/ramps.hpp>
#include <game/trees.hpp>

#include <psemek/gfx/program.hpp>

#include <set>
#include <map>
#include <vector>
#include <deque>
#include <optional>

inline int max_health(unit_type t)
{
	switch (t) {
	case unit_type::archer:
		return 100;
	case unit_type::pikeman:
		return 150;
	case unit_type::knight:
		return 250;
	}
	return 0;
}

inline int max_moves(unit_type t)
{
	switch (t) {
	case unit_type::archer:
		return 8;
	case unit_type::pikeman:
		return 12;
	case unit_type::knight:
		return 6;
	default:
		return 0;
	}
}

inline bool is_melee(unit_type t)
{
	switch (t) {
	case unit_type::pikeman:
	case unit_type::knight:
		return true;
	default:
		return false;
	}
}

inline int melee_damage(unit_type t)
{
	switch (t) {
	case unit_type::pikeman:
		return 40;
	case unit_type::knight:
		return 60;
	default:
		return 0;
	}
}

constexpr int slope_ramp_cost = 5;
constexpr int flat_ramp_cost = 10;
constexpr int tree_wood_amount = 20;

constexpr int arrow_damage = 20;

inline int ramp_cost(tile type)
{
	if (std::get_if<flat>(&type))
		return flat_ramp_cost;
	return slope_ramp_cost;
}

struct units
{
	units(map const & m, ramps & r, trees & t);

	int wood_amount(int player) const;

	using id = std::uint32_t;

	id add_unit(unit_type type, int player, geom::point<int, 2> const & tile, int dir);

	void render(render_options const & options);

	struct unit_info
	{
		unit_type type;
		int player;
		geom::point<int, 2> tile;
		int health;
	};

	std::optional<id> unit_at(geom::point<int, 2> const & tile) const;
	unit_info const & get(id unit_id) const;
	geom::point<float, 3> get_unit_position(id unit_id) const;

	std::vector<id> all_units() const;
	std::vector<id> player_units(int player) const;
	std::set<id> unit_group(id i) const;

	void move(id unit_id, int dir);

	void fire(id i, geom::point<int, 2> const & target);

	void melee(id i, geom::point<int, 2> const & target);

	void chop(id unit_id, int dir);

	void build(id unit_id, int dir, tile type);

	void update(float dt);

	bool has_animation() const;

private:
	map const & map_;
	ramps & ramps_;
	trees & trees_;

	int wood_amount_[2];

	struct arrow_data
	{
		geom::point<float, 2> start;
		geom::vector<float, 2> direction;
		float z_a, z_b, z_c;
		geom::point<int, 2> target;

		float total_time;
		float time = 0.f;
	};

	struct melee_data
	{
		geom::point<int, 2> target;
		float time = 0.f;
		bool attacked = false;
	};

	struct unit_data
	{
		unit_info info;
		geom::point<float, 2> position;
		std::deque<geom::point<int, 2>> movement_queue;
		bool new_move = true;
		float rotation;

		float damage_animation = 0.f;

		std::optional<arrow_data> delayed_arrow;
		std::optional<melee_data> delayed_melee;
		std::optional<geom::point<int, 2>> delayed_chop;
		std::optional<std::pair<geom::point<int, 2>, tile>> delayed_build;
	};

	id next_id_ = 0;
	std::map<id, unit_data> units_;

	static constexpr int unit_types = 3;

	gfx::program program_;
	gfx::mesh base_mesh_;
	gfx::texture_2d base_texture_;
	gfx::mesh mesh_[unit_types];
	gfx::texture_2d texture_[unit_types];

	std::vector<arrow_data> arrows_;

	gfx::mesh arrow_mesh_;
	gfx::texture_2d arrow_texture_;
};
