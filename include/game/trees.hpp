#pragma once

#include <game/map.hpp>
#include <game/render.hpp>

#include <psemek/gfx/program.hpp>

using namespace psemek;

struct trees
{
	trees(map const & m);

	bool has_tree(int x, int y) const;
	void remove_tree(int x, int y);

	void update(float dt);

	bool has_animation() const;

	void render(render_options const & options);

private:

	struct tree_data
	{
		geom::point<float, 3> position{};
		float size = 0.f;
		float rotation = 0.f;
		std::optional<float> dying;
	};

	util::array<tree_data, 2> trees_;
	int dying_trees_ = 0;

	gfx::program program_;
	gfx::mesh trunk_mesh_;
	gfx::mesh leaves_mesh_;
};
