#pragma once

#include <psemek/app/scene.hpp>

#include <memory>
#include <functional>

using namespace psemek;

std::unique_ptr<app::scene> make_settings_scene(std::function<void()> on_exit);
