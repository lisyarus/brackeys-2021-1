#pragma once

#include <psemek/app/scene.hpp>

#include <memory>
#include <functional>

using namespace psemek;

std::unique_ptr<app::scene> make_map_generation_scene(std::function<void()> on_exit, std::function<void(std::unique_ptr<app::scene>)> on_change_scene);
