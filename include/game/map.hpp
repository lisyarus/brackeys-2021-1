#pragma once

#include <psemek/gfx/color.hpp>

#include <psemek/random/generator.hpp>

#include <psemek/geom/point.hpp>
#include <psemek/geom/vector.hpp>

#include <psemek/util/array.hpp>

#include <variant>
#include <optional>
#include <vector>
#include <array>

using namespace psemek;

struct flat{};

inline bool operator == (flat, flat) { return true; }
inline bool operator != (flat, flat) { return false; }

struct slope
{
	int direction;
};

inline bool operator == (slope const & s1, slope const & s2)
{
	return s1.direction == s2.direction;
}

inline bool operator != (slope const & s1, slope const & s2)
{
	return !(s1 == s2);
}

using tile = std::variant<flat, slope>;

std::array<geom::point<float, 3>, 4> surface(tile t, int x, int y, int h);

static gfx::color_4f const player_colors[] =
{
	{0.f, 0.f, 1.f, 1.f},
	{1.f, 0.f, 0.f, 1.f},
};

enum class unit_type : int
{
	archer = 0,
	pikeman = 1,
	knight = 2,
};

static geom::vector<int, 2> const directions[4] =
{
	{1, 0},
	{0, 1},
	{-1, 0},
	{0, -1},
};

inline geom::vector<float, 3> light_dir()
{
	return geom::normalized(geom::vector{2.f, 1.f, 3.f});
}

struct map
{
	geom::vector<int, 2> size;
	util::array<int, 2> height;
	util::array<tile, 2> geometry;
	util::array<float, 2> tree_size;
	util::array<int, 2> bridges;

	std::vector<std::pair<unit_type, geom::point<int, 2>>> units[2];
	int wood_amount[2];

	std::optional<std::array<geom::point<float, 3>, 4>> tile_surface(int x, int y) const;
	std::optional<float> tile_center_height(int x, int y) const;
};

map generate_map(int width, int height, float trees_density, int river_width, int bridge_count, random::generator && rng, int army0, int army1);
