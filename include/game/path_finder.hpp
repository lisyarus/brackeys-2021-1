#pragma once

#include <game/map.hpp>
#include <game/trees.hpp>
#include <game/ramps.hpp>
#include <game/units.hpp>

#include <map>
#include <set>

struct path_finder
{
	path_finder(map const & m, trees const & t, ramps const & r, units const & u);

	bool passable(geom::point<int, 2> const & tile, bool ignore_units = false) const;

	struct reachable_info
	{
		int cost;
		int direction;
	};

	using reachable_map = std::map<geom::point<int, 2>, reachable_info>;

	reachable_map reachable(geom::point<int, 2> const & start, int moves) const;

	using tile_set = std::set<geom::point<int, 2>>;

	bool shootable(geom::point<int, 2> const & start, geom::point<int, 2> const & target) const;
	tile_set shootable(geom::point<int, 2> const & start) const;

	bool visible(geom::point<float, 3> const & start, geom::point<float, 3> const & end) const;
	bool visible(geom::point<float, 3> const & start, geom::point<float, 3> const & end, std::set<geom::point<int, 2>> const & ignored) const;

	struct melee_info
	{
		geom::point<int, 2> position;
		int cost;
	};

	using melee_map = std::map<geom::point<int, 2>, melee_info>;
	melee_map melee_attackable(reachable_map const & reachable, int moves) const;

	reachable_map choppable(reachable_map const & reachable, int moves) const;
	reachable_map buildable(reachable_map const & reachable, int moves) const;

private:
	map const & map_;
	trees const & trees_;
	ramps const & ramps_;
	units const & units_;
};
