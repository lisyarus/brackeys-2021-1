#pragma once

#include <psemek/gfx/mesh.hpp>
#include <psemek/gfx/texture.hpp>

#include <psemek/geom/matrix.hpp>
#include <psemek/geom/point.hpp>
#include <psemek/geom/vector.hpp>

#include <string_view>

using namespace psemek;

struct render_options
{
	float time;
	geom::matrix<float, 4, 4> camera_transform;
	geom::point<float, 3> camera_pos;
	geom::vector<float, 3> camera_dir;
	geom::vector<float, 3> light_dir;
};

gfx::mesh load_mesh(std::string_view data);

gfx::texture_2d load_texture(std::string_view data, bool linear = true);

gfx::texture_2d load_transparent_texture(std::string_view data, gfx::color_rgb const & transparent_color);
