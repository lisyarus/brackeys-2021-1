Day 1
+ Basic camera rotation
+ Terrain generation with ramps
+ Terrain rendering
+ Lakes (everything at 0 height is water)
+ River (straight line through midmap)

Day 2
+ Sparkling water
+ Tile picking with mouse
+ Camera movement (tile picking + RMB)
+ Trees (single rotated model)
+ Flat wooden ramps (same mesh as terrain + transparent texture)
+ Bridges (generated as sequences of ramps)
+ Archer model

Day 3
+ Slope ramps
+ Gameplay & UI basics: turn-based (vs primitive AI / vs another player), select unit & move

Day 4
+ Movement animations
+ Buttons (text/image + popup)
+ Full turn UI: end turn, attack, skip turn
+ Archery: archers attack at distance
+ Health & health bars
+ Separate move/shoot ui modes
+ Proper archer shooting range calculation

Day 5
+ Fix button selection bugs
+ Move camera back when free camera mode is enabled
+ Height-dependent archer's range
+ Archers rotate towards shooting direction
+ Units can chop wood
+ Victory state
+ Units can build & destroy ramps
+ Global per-player "wood" resource
+ Pikemen model & unit type

Day 6
+ Melee: soldiers attack adjacent soldiers
+ Melee: adjacent units attack together
+ Main menu
+ Map generation screen: map preview, select map size, river, trees amount
+ Swordsmen model & unit type
+ "How to play" page

Day 7
+ Map generation screen: army size settings
+ Better formation for large armies
+ Defensive AI: react when attacked by archers
+ Don't skip turn when selecting unit in move mode
+ Forbid building ramps where units already are
+ Support reselecting units
+ Button click sound
+ Unit select sound
? Unit move sound
+ Fire arrow sound
+ Arrow hit sound
+ Melee hit sound
+ Chop wood sound
+ Build ramp sound
* Battle start music
* Victory music
* Main theme soundtrack
* Fix AI stuck near ramps
