#include <game/sound.hpp>
#include <game/settings.hpp>

#include <psemek/audio/engine.hpp>

#include <sounds/click_mp3.hpp>
#include <sounds/select_mp3.hpp>
#include <sounds/move_mp3.hpp>
#include <sounds/fire_mp3.hpp>
#include <sounds/arrow_hit_mp3.hpp>
#include <sounds/melee_hit_mp3.hpp>
#include <sounds/chop_mp3.hpp>
#include <sounds/build_mp3.hpp>

using namespace psemek;

namespace
{
struct sound_impl
{
	audio::engine engine;
	std::shared_ptr<audio::track> click;
	std::shared_ptr<audio::track> select;
	std::shared_ptr<audio::track> move;
	std::shared_ptr<audio::track> fire;
	std::shared_ptr<audio::track> arrow_hit;
	std::shared_ptr<audio::track> melee_hit;
	std::shared_ptr<audio::track> chop;
	std::shared_ptr<audio::track> build;

	sound_impl()
	{
		click = engine.load(sounds::click_mp3);
		select = engine.load(sounds::select_mp3);
		move = engine.load(sounds::move_mp3);
		fire = engine.load(sounds::fire_mp3);
		arrow_hit = engine.load(sounds::arrow_hit_mp3);
		melee_hit = engine.load(sounds::melee_hit_mp3);
		chop = engine.load(sounds::chop_mp3);
		build = engine.load(sounds::build_mp3);
	}

	void play(std::shared_ptr<audio::track> track)
	{
		auto stream = engine.play(track, false);
		stream->volume(global_settings().sound_volume);
		stream->start();
	}
};

sound_impl & impl()
{
	static sound_impl i;
	return i;
}
}

void sound::click()
{
	impl().play(impl().click);
}

void sound::select()
{
	impl().play(impl().select);
}

void sound::move()
{
	impl().play(impl().move);
}

void sound::fire()
{
	impl().play(impl().fire);
}

void sound::arrow_hit()
{
	impl().play(impl().arrow_hit);
}

void sound::melee_hit()
{
	impl().play(impl().melee_hit);
}

void sound::chop()
{
	impl().play(impl().chop);
}

void sound::build()
{
	impl().play(impl().build);
}

