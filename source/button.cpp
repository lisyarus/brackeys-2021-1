#include <game/button.hpp>
#include <game/sound.hpp>

gfx::color_rgba const button::default_color = {117, 78, 50, 255};

button::button(std::string_view text, int width, int height, gfx::color_rgba color)
	: text_{text}
	, width_{width}
	, height_{height}
	, color_{color}
{}

button::button(gfx::texture_2d image, int scale, std::string_view helper_text)
	: text_{helper_text}
	, image_{std::move(image)}
	, width_{image_.width()}
	, height_{image_.height()}
	, scale_{scale}
{}

void button::set_position(geom::point<int, 2> const & position)
{
	position_ = position;
}

void button::set_on_click(std::function<void()> callback)
{
	callback_ = std::move(callback);
}

void button::set_active(bool active)
{
	active_ = active;
	if (!active_)
	{
		hover_ = false;
		down_ = false;
	}
}

void button::render(gfx::painter & painter)
{
	float cx = position_[0];
	float cy = position_[1];

	float w = width_ / 2 * scale_;
	float h = height_ / 2 * scale_;

	if (image_)
	{
		gfx::color_rgba color = {0, 0, 0, 0};
		if (down_)
			color = {0, 0, 0, 63};
		else if (hover_)
			color = {255, 255, 255, 63};

		painter.texture(image_, geom::box<float, 2>{{{cx - w, cx + w}, {cy - h, cy + h}}}, color);

		if (hover_)
		{
			gfx::painter::text_options opts;
			opts.scale = 2.f;
			opts.x = gfx::painter::x_align::right;

			opts.c = {0, 0, 0, 255};
			painter.text(geom::point<float, 2>{position_[0] + w, position_[1] + h + 16.f} + geom::vector{1.f, 1.f}, text_, opts);

			opts.c = {255, 255, 255, 255};
			painter.text(geom::point<float, 2>{position_[0] + w, position_[1] + h + 16.f}, text_, opts);
		}
	}
	else
	{
		gfx::color_rgba white{255, 255, 255, 255};
		gfx::color_rgba black{0, 0, 0, 255};

		auto color = color_;
		if (down_)
		{
			color = gfx::dark(color, 0.25f);
			white = gfx::dark(white, 0.25f);
			black = gfx::dark(black, 0.25f);
		}
		else if (hover_)
		{
			color = gfx::light(color, 0.25f);
			white = gfx::light(white, 0.25f);
			black = gfx::light(black, 0.25f);
		}

		painter.rect(geom::box<float, 2>{{{cx - w + 2.f, cx + w + 2.f}, {cy - h + 2.f, cy + h + 2.f}}}, black);
		painter.rect(geom::box<float, 2>{{{cx - w, cx + w}, {cy - h, cy + h}}}, white);
		painter.rect(geom::box<float, 2>{{{cx - w + 6.f, cx + w - 6.f}, {cy - h + 6.f, cy + h - 6.f}}}, black);
		painter.rect(geom::box<float, 2>{{{cx - w + 8.f, cx + w - 6.f}, {cy - h + 8.f, cy + h - 6.f}}}, color);

		gfx::painter::text_options opts;
		opts.scale = 3.f;

		opts.c = black;
		painter.text(geom::cast<float>(position_) + geom::vector{1.f, 1.f}, text_, opts);

		opts.c = white;
		painter.text(geom::cast<float>(position_), text_, opts);
	}
}

bool button::on_mouse_move(geom::point<int, 2> const & mouse)
{
	if (!active_)
		return false;

	if (std::abs(mouse[0] - position_[0]) <= width_ * scale_ / 2.f && std::abs(mouse[1] - position_[1]) <= height_ * scale_ / 2.f)
	{
		hover_ = true;
	}
	else
	{
		hover_ = false;
	}

	return hover_;
}

bool button::on_mouse_down()
{
	if (!active_)
		return false;

	if (hover_)
	{
		down_ = true;
		sound::click();
		if (callback_) callback_();
		return true;
	}

	return false;
}

bool button::on_mouse_up()
{
	if (!active_)
		return false;

	if (down_)
	{
		down_ = false;
		return true;
	}

	return false;
}
