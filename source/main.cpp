#include <game/main_menu_scene.hpp>
#include <game/settings.hpp>

#include <psemek/app/app.hpp>
#include <psemek/app/main.hpp>

#include <psemek/log/log.hpp>

#include <filesystem>

using namespace psemek;

struct game_app
	: app::app
{
	game_app(std::filesystem::path exe_path);
	~game_app();

private:
	std::filesystem::path settings_file_;
};

game_app::game_app(std::filesystem::path exe_path)
	: app("Untitled", 4)
{
	settings_file_ = exe_path.parent_path() / "settings.txt";
	global_settings().load(settings_file_);

	push_scene(make_main_menu_scene([this]{ stop(); }, *this));
}

game_app::~game_app()
{
	global_settings().save(settings_file_);
}

int main(int /* argc */, char ** argv)
{
	return app::main<game_app>(std::filesystem::path(argv[0]));
}
