#include <game/howto_scene.hpp>
#include <game/button.hpp>

#include <psemek/gfx/gl.hpp>
#include <psemek/gfx/painter.hpp>

#include <psemek/geom/camera.hpp>

namespace
{

static int const margin = 32;
static int const button_height = 64;

struct howto_scene
	: app::scene_base
{
	howto_scene(std::function<void()> on_quit);

	void on_resize(int width, int height) override;
	void on_mouse_move(int x, int y, int dx, int dy) override;
	void on_left_button_down() override;
	void on_left_button_up() override;

	void present();

private:
	std::function<void()> on_quit_;

	int font_size_ = 2;

	button back_button_;
	std::optional<button> panel_;
	std::vector<std::string> text_lines_;

	std::vector<button *> buttons_;

	gfx::painter painter_;
};

static std::string const text =
"Two players fight on a procedurally generated map. Each turn a player can select one of his units and "
"move, attack, chop trees, or build wooden ramps. If the selected unit didn't make a move, the player can select another unit.\n"
"\n"
"There are three unit types:\n"
"- Archers have medium speed and low HP. Their shooting range is affected by height & obstacles (terrain, units, trees, ramps).\n"
"- Pikemen have high speed and medium HP, and can melee attack enemies.\n"
"- Knights have low speed and high HP, and can melee attack enemies as well.\n"
"Several units of the same type forming a connected group attack together.\n"
"\n"
"Any unit can produce wood resource by chopping trees and use it to build wooden ramps. "
"Ramps can be built on water, on land, and on another flat ramps (but not on ladders!). "
"Ramps obstruct unit movement & increase archers' range, use them wisely.\n"
"\n"
"Use right mouse button to change camera target.\n"
"Use middle mouse button (or [Shift] + right mouse button) to rotate camera.\n"
"Use mouse wheel to zoom in/out.\n"
;

howto_scene::howto_scene(std::function<void()> on_quit)
	: on_quit_(std::move(on_quit))
	, back_button_("Back", 200, button_height)
{
	back_button_.set_on_click([this]{ on_quit_(); });

	buttons_ =
	{
		&back_button_
	};
}

void howto_scene::on_resize(int width, int height)
{
	scene_base::on_resize(width, height);
	gl::Viewport(0, 0, width, height);

	back_button_.set_position({back_button_.width() / 2 + margin, height - back_button_.height() / 2 - margin});

	panel_ = button("", width - 2 * margin, height - 3 * margin - back_button_.height());
	panel_->set_position({width / 2, margin + panel_->height() / 2});

	font_size_ = 2;

	while (true)
	{
		text_lines_.clear();

		int const chars_per_line = (panel_->width() - 2 * margin) / (9 * font_size_);
		int const max_lines = (panel_->height() - 2 * margin) / (12 * font_size_);

		text_lines_.emplace_back();
		for (auto it = text.begin();;)
		{
			auto & line = text_lines_.back();
			while (it < text.end() && std::isspace(*it))
			{
				if (line.empty())
					++it;
				else
					line.push_back(*it++);
			}

			if (it == text.end()) break;

			auto next = std::find_if(it, text.end(), [](char c){ return std::isspace(c); });
			if ((next - it > chars_per_line) || (next - it <= chars_per_line - static_cast<int>(line.size())))
			{
				line.insert(line.end(), it, next);
				it = next;
				while (it < text.end() && *it == '\n')
				{
					text_lines_.emplace_back();
					++it;
				}
			}
			else
				text_lines_.emplace_back();
		}

		if (text_lines_.back().empty())
			text_lines_.pop_back();

		if (font_size_ == 2 && text_lines_.size() > max_lines)
		{
			font_size_ = 1;
		}
		else
			break;
	}
}

void howto_scene::on_mouse_move(int x, int y, int dx, int dy)
{
	scene_base::on_mouse_move(x, y, dx, dy);

	for (auto & b : buttons_)
		b->on_mouse_move({x, y});
}

void howto_scene::on_left_button_down()
{
	scene_base::on_left_button_down();

	for (auto & b : buttons_)
		if (b->on_mouse_down()) return;
}

void howto_scene::on_left_button_up()
{
	scene_base::on_left_button_up();

	for (auto & b : buttons_)
		b->on_mouse_up();
}

void howto_scene::present()
{
	gl::ClearColor(0.8f, 0.8f, 1.f, 1.f);
	gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

	for (auto & b : buttons_)
		b->render(painter_);

	if (panel_)
		panel_->render(painter_);

	{
		gfx::painter::text_options opts;
		opts.scale = font_size_;
		opts.x = gfx::painter::x_align::left;
		opts.y = gfx::painter::y_align::top;

		for (int i = 0; i < text_lines_.size(); ++i)
		{
			float x = panel_->position()[0] - panel_->width() / 2 + margin;
			float y = panel_->position()[1] - panel_->height() / 2 + margin + i * (font_size_ * 12 + 4);

			opts.c = {0, 0, 0, 255};
			painter_.text({x + 1.f, y + 1.f}, text_lines_[i], opts);

			opts.c = {255, 255, 255, 255};
			painter_.text({x, y}, text_lines_[i], opts);
		}
	}

	painter_.render(geom::window_camera{width(), height()}.transform());
}

}

std::unique_ptr<app::scene> make_howto_scene(std::function<void()> on_quit)
{
	return std::make_unique<howto_scene>(std::move(on_quit));
}
