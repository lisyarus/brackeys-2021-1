#include <game/settings_scene.hpp>
#include <game/settings.hpp>
#include <game/button.hpp>
#include <game/sound.hpp>

#include <psemek/gfx/gl.hpp>
#include <psemek/gfx/painter.hpp>

#include <psemek/geom/camera.hpp>

#include <psemek/log/log.hpp>

namespace
{

static int const margin = 32;
static int const button_height = 64;

struct toggle_button
	: button
{
	static gfx::color_rgba const default_toggle_color;
	toggle_button(int width, int height, gfx::color_rgba color = default_color, gfx::color_rgba toggle_color = default_toggle_color);

	void set_on_state_change(std::function<void(bool)> on_state_change);

	void render(gfx::painter & painter) override;

	bool toggled() const { return state_; }
	void set_toggled(bool state) { state_ = state; }

private:
	gfx::color_rgba toggle_color_;
	bool state_ = false;
};

gfx::color_rgba const toggle_button::default_toggle_color = gfx::lerp(gfx::red.as_color_rgba(), button::default_color, 0.25f);

toggle_button::toggle_button(int width, int height, gfx::color_rgba color, gfx::color_rgba toggle_color)
	: button("", width, height, color)
	, toggle_color_{toggle_color}
{
	set_on_state_change([](bool){});
}

void toggle_button::set_on_state_change(std::function<void(bool)> on_state_change)
{
	set_on_click([this, on_state_change = std::move(on_state_change)]{
		state_ = !state_;
		if (on_state_change)
			on_state_change(state_);
	});
}

void toggle_button::render(gfx::painter & painter)
{
	button::render(painter);

	if (!state_) return;

	float cx = position_[0];
	float cy = position_[1];

	float w = width_ / 2 * scale_;
	float h = height_ / 2 * scale_;

	auto color = toggle_color_;
	if (down_)
		color = gfx::dark(color, 0.25f);
	else if (hover_)
		color = gfx::light(color, 0.25f);

	float s = 10.f;

	painter.rect(geom::box<float, 2>{{{cx - w + s, cx + w - s + 1}, {cy - h + s, cy + h - s + 1}}}, color);
}

struct slider
{
	slider(int width, int height);

	void set_width(int width){ width_ = width; }

	void set_on_value_changed(std::function<void(float)> on_value_changed)
	{
		on_value_changed_ = on_value_changed;
	}

	int width() const { return width_; }
	int height() const { return height_; }

	void set_position(int x, int y){
		x_ = x;
		y_ = y;
	}

	float value() const { return value_; }
	void set_value(float value) { value_ = value; }

	bool on_mouse_move(geom::point<int, 2> const & mouse);
	bool on_mouse_down();
	bool on_mouse_up();

	void render(gfx::painter & painter);

private:
	int width_;
	int height_;
	int x_ = 0, y_ = 0;
	float value_ = 0.f;

	bool hover_ = false;
	bool down_ = false;

	std::optional<geom::point<int, 2>> mouse_;
	std::optional<geom::point<int, 2>> drag_;

	std::function<void(float)> on_value_changed_;
};

slider::slider(int width, int height)
	: width_(width)
	, height_(height)
{}

bool slider::on_mouse_move(geom::point<int, 2> const & mouse)
{
	mouse_ = mouse;
	if (drag_)
	{
		drag_ = mouse;
		value_ = ((*mouse_)[0] - (x_ - width_ / 2)) / (1.f * width_);
		value_ = geom::clamp(value_, {0.f, 1.f});
		if (on_value_changed_) on_value_changed_(value_);
		return true;
	}
	hover_ = (mouse[0] >= x_ - width_ / 2) && (mouse[0] <= x_ + width_ / 2)
		&& (mouse[1] >= y_ - height_ / 2) && (mouse[1] <= y_ + height_ / 2);
	if (!hover_)
		down_ = false;
	return hover_;
}

bool slider::on_mouse_down()
{
	if (hover_ && mouse_)
	{
		down_ = true;
		drag_ = mouse_;
		value_ = ((*mouse_)[0] - (x_ - width_ / 2)) / (1.f * width_);
		value_ = geom::clamp(value_, {0.f, 1.f});
		if (on_value_changed_) on_value_changed_(value_);
		sound::click();
		return true;
	}
	return false;
}

bool slider::on_mouse_up()
{
	if (down_ || drag_)
	{
		down_ = false;
		drag_ = std::nullopt;
		return true;
	}
	return false;
}

void slider::render(gfx::painter & painter)
{
	gfx::color_rgba axis_color = gfx::light(button::default_color);
	geom::box<float, 2> axis_box;
	axis_box[0].min = x_ - width_ / 2;
	axis_box[0].max = x_ + width_ / 2;
	axis_box[1].min = y_ - height_ / 8;
	axis_box[1].max = y_ + height_ / 8;
	painter.rect(axis_box, axis_color);

	int x = x_ - width_ / 2 + value_ * width_;
	gfx::color_rgba color = gfx::light(button::default_color, 0.75f);
	if (hover_ && !down_)
		color = gfx::light(color, 0.75f);
	else if (hover_ && down_)
		color = gfx::dark(color, 0.25f);
	geom::box<float, 2> box;
	box[0].min = x - 5.f;
	box[0].max = x + 5.f;
	box[1].min = y_ - height_ / 3;
	box[1].max = y_ + height_ / 3;
	painter.rect(box, color);
}

struct settings_scene
	: app::scene_base
{
	settings_scene(std::function<void()> on_quit);

	void on_resize(int width, int height) override;
	void on_mouse_move(int x, int y, int dx, int dy) override;
	void on_left_button_down() override;
	void on_left_button_up() override;

	void present();

private:
	std::function<void()> on_quit_;

	button back_button_;
	std::optional<button> panel_;

	toggle_button autocamera_toggle_;
	toggle_button inverted_y_toggle_;
	toggle_button inverted_wheel_toggle_;

	slider mouse_sensitivity_slider_;
	slider sound_volume_slider_;

	std::vector<button *> buttons_;

	gfx::painter painter_;
};

settings_scene::settings_scene(std::function<void()> on_quit)
	: on_quit_(std::move(on_quit))
	, back_button_("Back", 200, button_height)
	, autocamera_toggle_(40, 40)
	, inverted_y_toggle_(40, 40)
	, inverted_wheel_toggle_(40, 40)
	, mouse_sensitivity_slider_(100, 40)
	, sound_volume_slider_(100, 40)
{
	back_button_.set_on_click([this]{ on_quit_(); });

	autocamera_toggle_.set_toggled(global_settings().auto_camera);
	autocamera_toggle_.set_on_state_change([](bool value){
		global_settings().auto_camera = value;
	});

	inverted_y_toggle_.set_toggled(global_settings().inverted_y);
	inverted_y_toggle_.set_on_state_change([](bool value){
		global_settings().inverted_y = value;
	});

	inverted_wheel_toggle_.set_toggled(global_settings().inverted_wheel);
	inverted_wheel_toggle_.set_on_state_change([](bool value){
		global_settings().inverted_wheel = value;
	});

	buttons_ =
	{
		&back_button_,
		&autocamera_toggle_,
		&inverted_y_toggle_,
		&inverted_wheel_toggle_,
	};

	mouse_sensitivity_slider_.set_value(global_settings().mouse_sensitivity);
	mouse_sensitivity_slider_.set_on_value_changed([](float value){
		global_settings().mouse_sensitivity = value;
	});

	sound_volume_slider_.set_value(global_settings().sound_volume);
	sound_volume_slider_.set_on_value_changed([](float value){
		global_settings().sound_volume = value;
	});
}

void settings_scene::on_resize(int width, int height)
{
	scene_base::on_resize(width, height);
	gl::Viewport(0, 0, width, height);

	back_button_.set_position({back_button_.width() / 2 + margin, height - back_button_.height() / 2 - margin});

	panel_ = button("", width - 2 * margin, height - 3 * margin - back_button_.height());
	panel_->set_position({width / 2, margin + panel_->height() / 2});

	int row_h = 48;

	int x = panel_->position()[0];
	int y = panel_->position()[1] - panel_->height() / 2 + margin + row_h / 2;

	autocamera_toggle_.set_position({x - autocamera_toggle_.width(), y + 0 * row_h});
	inverted_y_toggle_.set_position({x - inverted_y_toggle_.width(), y + 1 * row_h});
	inverted_wheel_toggle_.set_position({x - inverted_wheel_toggle_.width(), y + 2 * row_h});

	mouse_sensitivity_slider_.set_width(panel_->width() / 2 - margin - 20);
	mouse_sensitivity_slider_.set_position(x - mouse_sensitivity_slider_.width() / 2 - 20, y + 3 * row_h);

	sound_volume_slider_.set_width(panel_->width() / 2 - margin - 20);
	sound_volume_slider_.set_position(x - mouse_sensitivity_slider_.width() / 2 - 20, y + 4 * row_h);
}

void settings_scene::on_mouse_move(int x, int y, int dx, int dy)
{
	scene_base::on_mouse_move(x, y, dx, dy);

	for (auto & b : buttons_)
		b->on_mouse_move({x, y});

	mouse_sensitivity_slider_.on_mouse_move({x, y});
	sound_volume_slider_.on_mouse_move({x, y});
}

void settings_scene::on_left_button_down()
{
	scene_base::on_left_button_down();

	for (auto & b : buttons_)
		if (b->on_mouse_down()) return;

	if (mouse_sensitivity_slider_.on_mouse_down()) return;
	if (sound_volume_slider_.on_mouse_down()) return;
}

void settings_scene::on_left_button_up()
{
	scene_base::on_left_button_up();

	for (auto & b : buttons_)
		b->on_mouse_up();

	mouse_sensitivity_slider_.on_mouse_up();
	sound_volume_slider_.on_mouse_up();
}

void settings_scene::present()
{
	gl::ClearColor(0.8f, 0.8f, 1.f, 1.f);
	gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

	if (panel_)
		panel_->render(painter_);

	for (auto & b : buttons_)
		b->render(painter_);

	mouse_sensitivity_slider_.render(painter_);
	sound_volume_slider_.render(painter_);

	int row_h = 48;

	int x = panel_->position()[0];
	int y = panel_->position()[1] - panel_->height() / 2 + margin + row_h / 2;

	std::string_view lines[] =
	{
		"Automatic camera movement",
		"Inverted Y",
		"Inverted wheel",
		"Mouse sensitivity",
		"Sound volume",
	};

	gfx::painter::text_options opts;
	opts.scale = 2.f;
	opts.x = gfx::painter::x_align::left;
	opts.y = gfx::painter::y_align::center;

	for (auto l : lines)
	{
		opts.c = {0, 0, 0, 255};
		painter_.text({x + 1, y + 1}, l, opts);

		opts.c = {255, 255, 255, 255};
		painter_.text({x, y}, l, opts);

		y += row_h;
	}

	painter_.render(geom::window_camera{width(), height()}.transform());
}

}

std::unique_ptr<app::scene> make_settings_scene(std::function<void()> on_quit)
{
	return std::make_unique<settings_scene>(std::move(on_quit));
}
