#include <game/render.hpp>

#include <psemek/util/memory_stream.hpp>
#include <psemek/gfx/obj_parser.hpp>

gfx::mesh load_mesh(std::string_view data)
{
	util::memory_istream is{data};
	auto triangles = gfx::parse_obj(is).triangles;

	for (auto & t : triangles)
		for (auto & v : t.points)
			v.texcoord[1] = 1.f - v.texcoord[1];

	gfx::mesh result;
	result.setup<geom::point<float, 3>, geom::vector<float, 2>, geom::vector<float, 3>>();
	result.load(triangles, gl::STATIC_DRAW);

	return result;
}

gfx::texture_2d load_texture(std::string_view data, bool linear)
{
	util::memory_istream is{data};
	gfx::texture_2d result;
	result.load(gfx::read_ppm(is));
	if (linear)
	{
		result.linear_mipmap_filter();
		result.anisotropy();
		result.generate_mipmap();
	}
	else
		result.nearest_filter();
	return result;
}

gfx::texture_2d load_transparent_texture(std::string_view data, gfx::color_rgb const & transparent_color)
{
	util::memory_istream is{data};
	auto pm = util::map([&](gfx::color_rgb const & c){
		if (c == transparent_color)
			return gfx::color_rgba{c[0], c[1], c[2], 0};
		else
			return gfx::color_rgba{c[0], c[1], c[2], 255};
	}, gfx::read_ppm(is));

	gfx::texture_2d result;
	result.load(pm);
	result.nearest_filter();
	return result;
}
