#include <game/player.hpp>
#include <psemek/random/generator.hpp>
#include <psemek/random/uniform.hpp>

#include <psemek/util/clock.hpp>

namespace
{

struct human_controller
	: player_controller
{
	human_controller(int player, units const & u)
		: player_(player)
		, units_{u}
	{}

	std::string_view name() const override
	{
		return "Human";
	}

	void set_game_state(game_state const & state)
	{
		state_ = &state;
	}

	void on_human_action(player_action const & action) override
	{
		action_ = action;
	}

	bool free_camera() override
	{
		return !std::get_if<states::start_game>(state_);
	}

	bool show_ui() override
	{
		return true;
	}

	std::optional<player_action> action() override
	{
		auto a = action_;
		action_.reset();
		return a;
	}

private:
	int player_;
	units const & units_;
	game_state const * state_ = nullptr;
	std::optional<geom::point<int, 2>> selected_tile_;
	std::optional<player_action> action_;
};

struct ai_controller
	: player_controller
{
	ai_controller(int player, units const & u, path_finder const & pf, random::generator rng, bool offensive)
		: player_(player)
		, units_{u}
		, pf_{pf}
		, rng_{std::move(rng)}
		, offensive_{offensive}
		, name_{offensive ? "AI (offensive)" : "AI (defensive)"}
	{}

	std::string_view name() const override
	{
		return name_;
	}

	void set_game_state(game_state const & state)
	{
		state_ = &state;
	}

	void on_human_action(player_action const &) override
	{}

	bool free_camera() override
	{
		return false;
	}

	bool show_ui() override
	{
		return false;
	}

	std::optional<player_action> action() override
	{
		if (!action_)
		{
			clock_.restart();
			action_ = select_action();
		}

		if (clock_.count() < 0.5f)
			return std::nullopt;

		auto action = action_;
		action_ = std::nullopt;
		return action;
	}

private:
	int player_;
	units const & units_;
	path_finder const & pf_;
	game_state const * state_ = nullptr;
	random::generator rng_;
	bool const offensive_;
	std::string const name_;

	std::optional<player_action> action_;
	geom::point<int, 2> melee_position_;
	geom::point<int, 2> target_;
	bool attack_ = false;

	util::clock<std::chrono::duration<float>> clock_;

	player_action select_action();
};

player_action ai_controller::select_action()
{
	if (!state_)
		return actions::skip{};

	if (std::get_if<states::select>(state_))
	{
		auto const us = units_.player_units(player_);
		if (us.empty())
			return actions::skip{};

		auto const they = units_.player_units(1 - player_);
		if (they.empty())
			return actions::skip{};

		// Try to melee attack first

		std::vector<std::tuple<units::id, geom::point<int, 2>, geom::point<int, 2>>> melee;

		for (auto id : us)
		{
			if (!is_melee(units_.get(id).type)) continue;

			int moves = max_moves(units_.get(id).type);

			auto r = pf_.reachable(units_.get(id).tile, moves);
			auto a = pf_.melee_attackable(r, moves);

			std::optional<units::id> least_hp;
			geom::point<int, 2> target, position;
			int hp = 1000;
			for (auto jd : they)
			{
				if (a.count(units_.get(jd).tile))
				{
					int h = units_.get(jd).health;
					if (h < hp)
					{
						least_hp = jd;
						position = a[units_.get(jd).tile].position;
						target = units_.get(jd).tile;
						hp = h;
					}
				}
			}
			if (least_hp)
				melee.push_back({id, position, target});
		}

		if (!melee.empty())
		{
			auto i = random::uniform_distribution<int>(0, melee.size() - 1)(rng_);
			attack_ = true;
			melee_position_ = std::get<1>(melee[i]);
			target_ = std::get<2>(melee[i]);
			return actions::select{std::get<0>(melee[i])};
		}

		// Next, try to shoot

		std::vector<std::tuple<units::id, geom::point<int, 2>>> shoot;

		for (auto id : us)
		{
			if (units_.get(id).type != unit_type::archer) continue;

			auto s = pf_.shootable(units_.get(id).tile);

			std::optional<units::id> least_hp;
			geom::point<int, 2> target;
			int hp = 1000;
			for (auto jd : they)
			{
				if (s.count(units_.get(jd).tile))
				{
					int h = units_.get(jd).health;
					if (h < hp)
					{
						least_hp = jd;
						target = units_.get(jd).tile;
						hp = h;
					}
				}
			}

			if (least_hp)
				shoot.push_back({id, target});
		}

		if (!shoot.empty())
		{
			auto i = random::uniform_distribution<int>(0, shoot.size() - 1)(rng_);
			attack_ = true;
			target_ = std::get<1>(shoot[i]);
			return actions::select{std::get<0>(shoot[i])};
		}

		// Otherwise, try to move

		attack_ = false;

		bool under_fire = false;
		if (!offensive_)
		{
			// Check if we are under enemy archers' fire

			for (auto id : they)
			{
				if (units_.get(id).type != unit_type::archer) continue;

				for (auto jd : us)
				{
					if (pf_.shootable(units_.get(id).tile, units_.get(jd).tile))
					{
						under_fire = true;
						break;
					}
				}

				if (under_fire)
					break;
			}
		}

		if (offensive_ || under_fire)
		{
			for (int iteration = 0; iteration < 2 * us.size(); ++iteration)
			{
				auto id = us[random::uniform_distribution<int>(0, us.size() - 1)(rng_)];
				auto r = pf_.reachable(units_.get(id).tile, 1000);

				std::optional<geom::point<int, 2>> nearest;
				int distance = 1000;
				for (auto jd : they)
				{
					auto t = units_.get(jd).tile;
					for (int d = 0; d < 4; ++d)
					{
						auto tt = t + directions[d];

						if (r.count(tt) > 0)
						{
							int d = r[tt].cost;
							if (d > 0 && d < distance)
							{
								distance = d;
								nearest = tt;
							}
						}
					}
				}

				if (nearest)
				{
					int moves = max_moves(units_.get(id).type);

					auto p = *nearest;
					while (r[p].cost > moves)
						p -= directions[r[p].direction];
					target_ = p;

					return actions::select{id};
				}
			}
		}

		return actions::skip{};
	}
	else if (auto st = std::get_if<states::move>(state_))
	{
		if (attack_)
		{
			if (units_.get(st->selected_unit).type == unit_type::archer)
				return actions::shoot{target_};
			return actions::melee{melee_position_, target_};
		}

		return actions::move{target_};
	}

	return actions::skip{};
}

struct dummy_ai_controller
	: player_controller
{
	std::string_view name() const override
	{
		return "AI (dummy)";
	}

	void set_game_state(game_state const &) override
	{}

	void on_human_action(player_action const &) override
	{}

	bool free_camera() override
	{
		return false;
	}

	bool show_ui() override
	{
		return false;
	}

	std::optional<player_action> action() override
	{
		return actions::skip{};
	}
};

}

std::unique_ptr<player_controller> make_human_controller(int player, units const & u)
{
	return std::make_unique<human_controller>(player, u);
}

std::unique_ptr<player_controller> make_offensive_ai_controller(int player, units const & u, path_finder const & pf, random::generator rng)
{
	return std::make_unique<ai_controller>(player, u, pf, std::move(rng), true);
}

std::unique_ptr<player_controller> make_defensive_ai_controller(int player, units const & u, path_finder const & pf, random::generator rng)
{
	return std::make_unique<ai_controller>(player, u, pf, std::move(rng), false);
}

std::unique_ptr<player_controller> make_dummy_ai_controller()
{
	return std::make_unique<dummy_ai_controller>();
}
