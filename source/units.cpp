#include <game/units.hpp>
#include <game/sound.hpp>

#include <psemek/geom/quaternion.hpp>
#include <psemek/geom/rotation.hpp>
#include <psemek/geom/swizzle.hpp>

#include <models/unit_base_obj.hpp>
#include <models/archer_obj.hpp>
#include <models/archer_texture_ppm.hpp>
#include <models/arrow_obj.hpp>
#include <models/pikeman_obj.hpp>
#include <models/pikeman_texture_ppm.hpp>
#include <models/knight_obj.hpp>
#include <models/knight_texture_ppm.hpp>

#include <deque>

static char const units_vs[] =
R"(#version 330

uniform mat4 u_camera_transform;
uniform vec3 u_position;
uniform vec4 u_orientation;
uniform vec3 u_light_dir;

layout (location = 0) in vec3 in_position;
layout (location = 1) in vec2 in_texcoord;
layout (location = 2) in vec3 in_normal;

out vec2 texcoord;
out float light;

vec4 qprod(vec4 q1, vec4 q2)
{
	return vec4(q1.x * q2.x - dot(q1.yzw, q2.yzw), q1.x * q2.yzw + q2.x * q1.yzw + cross(q1.yzw, q2.yzw));
}

vec3 qrot(vec4 q, vec3 v)
{
	return qprod(qprod(q, vec4(0.0, v)), vec4(q.x, -q.yzw)).yzw;
}

void main()
{
	vec3 position = u_position + qrot(u_orientation, in_position);
	gl_Position = u_camera_transform * vec4(position, 1.0);

	vec3 normal = qrot(u_orientation, in_normal);
	light = 0.5 + 0.5 * dot(normal, u_light_dir);

	texcoord = in_texcoord;
}
)";

static char const units_fs[] =
R"(#version 330

uniform sampler2D u_texture;
uniform vec4 u_color;

in vec2 texcoord;
in float light;

out vec4 out_color;

void main()
{
	vec3 color = light * texture(u_texture, vec2(texcoord.x, texcoord.y)).rgb;
	color = mix(color, u_color.rgb, u_color.a);
	out_color = vec4(color, 1.0);
}
)";

static const float arrow_speed = 15.f;
static const float attack_duration = 0.25f;
static const float attack_offset = 0.25f;

units::units(map const & m, ramps & r, trees & t)
	: map_{m}
	, ramps_{r}
	, trees_{t}
	, program_{units_vs, units_fs}
{
	base_mesh_ = load_mesh(models::unit_base_obj);
	mesh_[0] = load_mesh(models::archer_obj);
	arrow_mesh_ = load_mesh(models::arrow_obj);
	mesh_[1] = load_mesh(models::pikeman_obj);
	mesh_[2] = load_mesh(models::knight_obj);

	{
		gfx::pixmap_rgb pm({1, 1}, gfx::color_rgb{255, 255, 255});
		base_texture_.load(pm);
		base_texture_.nearest_filter();
	}

	texture_[0] = load_texture(models::archer_texture_ppm);
	texture_[1] = load_texture(models::pikeman_texture_ppm);
	texture_[2] = load_texture(models::knight_texture_ppm);

	{
		gfx::pixmap_rgb pm({2, 1});
		pm(0, 0) = gfx::color_rgb{255, 255, 255};
		pm(1, 0) = gfx::color_rgb{63, 31, 0};
		arrow_texture_.load(pm);
		arrow_texture_.nearest_filter();
	}

	for (int player = 0; player < 2; ++player)
	{
		for (auto const & v : m.units[player])
		{
			add_unit(v.first, player, v.second, player * 2);
		}
	}

	wood_amount_[0] = m.wood_amount[0];
	wood_amount_[1] = m.wood_amount[1];
}

int units::wood_amount(int player) const
{
	return wood_amount_[player];
}

units::id units::add_unit(unit_type type, int player, geom::point<int, 2> const & tile, int dir)
{
	units::id id = next_id_++;
	unit_data & u = units_[id];
	u.info.type = type;
	u.info.player = player;
	u.info.tile = tile;
	u.info.health = max_health(type);
	u.position = {tile[0] + 0.5f, tile[1] + 0.5f};
	u.rotation = dir * 0.5f * geom::pi;
	return id;
}

void units::render(render_options const & options)
{
	program_.bind();
	program_["u_camera_transform"] = options.camera_transform;
	program_["u_light_dir"] = options.light_dir;
	program_["u_texture"] = 0;

	gl::ActiveTexture(gl::TEXTURE0);

	base_mesh_.bind();
	base_texture_.bind();
	for (auto const & p : units_)
	{
		unit_data const & u = p.second;
		gfx::color_4f color = player_colors[u.info.player];
		color[3] = 0.3f;

		geom::point<float, 3> position = geom::swizzle<0, 1, -1>(u.position);
		position[2] = ramps_.height_at(position[0], position[1]);

		auto orientation = geom::quaternion_rotation<float>(u.rotation, {0.f, 0.f, 1.f}).quat;

		auto normal = ramps_.normal_at(position[0], position[1]);
		auto const z = geom::vector{0.f, 0.f, 1.f};

		float angle = std::acos(normal[2]);
		auto axis = geom::normalized(geom::cross(z, normal));

		if (angle > 0.1f)
			orientation = geom::quaternion_rotation<float>(angle, axis).quat * orientation;

		if (u.delayed_melee && u.delayed_melee->time <= attack_duration)
		{
			auto delta = geom::swizzle<0, 1, -1>(geom::cast<float>(u.delayed_melee->target - u.info.tile));
			delta = geom::normalized(delta);

			position += delta * attack_offset * static_cast<float>(std::sin(u.delayed_melee->time / attack_duration * geom::pi));
		}

		program_["u_color"] = color;
		program_["u_position"] = position;
		program_["u_orientation"] = orientation;
		gl::DrawArrays(gl::TRIANGLES, 0, base_mesh_.vertex_count());
	}

	for (int t = 0; t < unit_types; ++t)
	{
		mesh_[t].bind();
		texture_[t].bind();
		for (auto const & p : units_)
		{
			unit_data const & u = p.second;
			if (static_cast<int>(u.info.type) != t) continue;

			gfx::color_4f color = player_colors[u.info.player];
			color[3] = 0.3f;

			if (u.damage_animation > 0.f)
				color = lerp(color, gfx::color_4f{1.f, 1.f, 1.f, 1.f}, u.damage_animation);

			geom::point<float, 3> position = geom::swizzle<0, 1, -1>(u.position);
			position[2] = ramps_.height_at(position[0], position[1]);

			auto orientation = geom::quaternion_rotation<float>(u.rotation, {0.f, 0.f, 1.f}).quat;

			if (u.delayed_melee && u.delayed_melee->time <= attack_duration)
			{
				auto delta = geom::swizzle<0, 1, -1>(geom::cast<float>(u.delayed_melee->target - u.info.tile));
				delta = geom::normalized(delta);

				position += delta * attack_offset * static_cast<float>(std::sin(u.delayed_melee->time / attack_duration * geom::pi));
			}

			program_["u_color"] = color;
			program_["u_position"] = position;
			program_["u_orientation"] = orientation;
			gl::DrawArrays(gl::TRIANGLES, 0, mesh_[t].vertex_count());
		}
	}

	arrow_mesh_.bind();
	arrow_texture_.bind();
	for (auto const & a : arrows_)
	{
		auto position = geom::swizzle<0, 1, -1>(a.start + a.direction * a.time * arrow_speed);
		position[2] = a.z_a * a.time * a.time + a.z_b * a.time + a.z_c;

		auto direction = geom::swizzle<0, 1, -1>(a.direction * arrow_speed);
		direction[2] = 2.f * a.z_a * a.time + a.z_b;
		direction = geom::normalized(direction);

		float r = std::atan2(direction[1], direction[0]);

		auto orientation = geom::quaternion_rotation<float>(r, {0.f, 0.f, 1.f}).quat;

		if (std::abs(direction[2]) > 0.01f)
		{
			auto dir_2d = geom::swizzle<0, 1, -1>(direction);
			auto axis = geom::normalized(geom::cross(dir_2d, direction));
			float s = std::acos(geom::dot(dir_2d, direction));

			orientation = geom::quaternion_rotation<float>(s, axis).quat * orientation;
		}

		program_["u_color"] = gfx::color_4f{1.f, 1.f, 1.f, 0.f};
		program_["u_position"] = position;
		program_["u_orientation"] = orientation;
		gl::DrawArrays(gl::TRIANGLES, 0, arrow_mesh_.vertex_count());
	}
}

std::optional<units::id> units::unit_at(geom::point<int, 2> const & tile) const
{
	for (auto const & p : units_)
	{
		if (p.second.info.health <= 0) continue;
		if (p.second.info.tile == tile)
			return p.first;
	}
	return std::nullopt;
}

units::unit_info const & units::get(id i) const
{
	return units_.at(i).info;
}

geom::point<float, 3> units::get_unit_position(id i) const
{
	geom::point<float, 3> position = geom::swizzle<0, 1, -1>(units_.at(i).position);
	position[2] = ramps_.height_at(position[0], position[1]);
	return position;
}

std::vector<units::id> units::all_units() const
{
	std::vector<id> result;
	for (auto const & p : units_)
	{
		if (p.second.info.health <= 0) continue;
		result.push_back(p.first);
	}
	return result;
}

std::vector<units::id> units::player_units(int player) const
{
	std::vector<id> result;
	for (auto const & p : units_)
	{
		if (p.second.info.health <= 0) continue;
		if (p.second.info.player == player)
			result.push_back(p.first);
	}
	return result;
}

std::set<units::id> units::unit_group(id i) const
{
	std::set<id> result;
	result.insert(i);

	std::deque<id> queue;
	queue.push_back(i);

	int const player = units_.at(i).info.player;
	auto const type = units_.at(i).info.type;

	while (!queue.empty())
	{
		auto i = queue.front();
		queue.pop_front();

		for (int dx = -1; dx <= 1; ++dx)
		{
			for (int dy = -1; dy <= 1; ++dy)
			{
				if (dx == 0 && dy == 0) continue;
				geom::vector d{dx, dy};

				auto t = units_.at(i).info.tile + d;
				if (auto j = unit_at(t))
				{
					if (units_.at(*j).info.health <= 0) continue;
					auto const & u = units_.at(*j);
					if (result.count(*j) == 0 && u.info.player == player && u.info.type == type)
					{
						result.insert(*j);
						queue.push_back(*j);
					}
				}
			}
		}
	}

	return result;
}

void units::move(id i, int dir)
{
	unit_data & u = units_.at(i);
	auto p = u.info.tile + directions[dir];
	u.info.tile = p;
	u.movement_queue.push_back(p);
}

void units::fire(id i, geom::point<int, 2> const & target)
{
	static float const arrow_start_height = 0.85f;
	static float const arrow_end_height = 0.75f;
	static float const arrow_max_z_delta = 1.f;

	auto start = units_.at(i).info.tile;

	arrow_data a;

	a.start[0] = start[0] + 0.5f;
	a.start[1] = start[1] + 0.5f;

	geom::point<float, 2> end;
	end[0] = target[0] + 0.5f;
	end[1] = target[1] + 0.5f;

	a.direction = geom::normalized(end - a.start);

	a.target = target;

	a.total_time = geom::distance(end, a.start) / arrow_speed;

	float z0 = ramps_.height_at(a.start[0], a.start[1]) + arrow_start_height;
	float z1 = ramps_.height_at(end[0], end[1]) + arrow_end_height;
	float zmax = z0 + arrow_max_z_delta;

	a.z_c = z0;

	if (auto s = geom::solve_quadratic(- a.total_time * a.total_time / 4.f / (zmax - z0), a.total_time, z0 - z1))
	{
		a.z_b = s->second;
		a.z_a = - a.z_b * a.z_b / 4.f / (zmax - z0);
	}
	else
	{
		return;
	}

	units_.at(i).delayed_arrow = a;
}

void units::melee(id i, geom::point<int, 2> const & target)
{
	unit_data & u = units_.at(i);
	u.delayed_melee = melee_data{target};
}

void units::chop(id i, int dir)
{
	unit_data & u = units_.at(i);
	u.delayed_chop = u.info.tile + directions[dir];
}

void units::build(id i, int dir, tile type)
{
	unit_data & u = units_.at(i);
	u.delayed_build = {u.info.tile + directions[dir], type};
}

void units::update(float dt)
{
	static float const rotation_speed = 15.f;
	static float const movement_speed = 25.f;

	std::vector<id> kill;

	for (auto & p : units_)
	{
		unit_data & u = p.second;

		if (u.damage_animation > 0.f)
			u.damage_animation -= dt * 2.f;
		else if (u.info.health <= 0)
			kill.push_back(p.first);

		auto rotate_to = [&](geom::point<float, 2> const & target)
		{
			auto dp = target - u.position;
			float dp_len = geom::length(dp);

			auto tgt_dir = geom::normalized(dp);
			float tgt_rotation = std::atan2(tgt_dir[1], tgt_dir[0]);

			auto dir = geom::vector{std::cos(u.rotation), std::sin(u.rotation)};

			float dot = geom::dot(dir, tgt_dir);
			float det = geom::det(dir, tgt_dir);

			if (dp_len < 0.01f || dot > 0.999f)
			{
				if (dp_len >= 0.01f)
					u.rotation = tgt_rotation;
				return true;
			}

			u.rotation += std::atan2(det, dot) * std::min(1.f, rotation_speed * dt);
			return false;
		};

		auto rotate_to_tile = [&](geom::point<int, 2> const & target)
		{
			return rotate_to(geom::cast<float>(target) + geom::vector{0.5f, 0.5f});
		};

		if (!u.movement_queue.empty())
		{
			if (rotate_to_tile(u.movement_queue.front()))
			{
				auto target = geom::cast<float>(u.movement_queue.front()) + geom::vector{0.5f, 0.5f};
				auto dp = target - u.position;

				auto distance = geom::length(dp);

				if (u.new_move && distance <= 0.5f)
				{
					sound::move();
					u.new_move = false;
				}

				if (distance < 0.01f)
				{
					u.position = target;
					u.movement_queue.pop_front();
					u.new_move = true;
				}
				else
				{
					u.position += dp * std::min(1.f, movement_speed * dt);
				}
			};
		}
		else if (u.delayed_arrow)
		{
			if (rotate_to(u.position + u.delayed_arrow->direction))
			{
				arrows_.push_back(*u.delayed_arrow);
				u.delayed_arrow = std::nullopt;
				sound::fire();
			}
		}
		else if (u.delayed_melee)
		{
			if (rotate_to_tile(u.delayed_melee->target))
			{
				u.delayed_melee->time += dt;
				if (u.delayed_melee->time >= attack_duration / 2.f && !u.delayed_melee->attacked)
				{
					u.delayed_melee->attacked = true;
					auto id = unit_at(u.delayed_melee->target);
					if (id)
					{
						units_.at(*id).info.health -= melee_damage(u.info.type);
						units_.at(*id).damage_animation = 1.f;
						sound::melee_hit();
					}
				}

				if (u.delayed_melee->time >= attack_duration)
				{
					u.delayed_melee = std::nullopt;
				}
			}
		}
		else if (u.delayed_chop)
		{
			auto t = *u.delayed_chop;

			if (rotate_to_tile(t))
			{
				trees_.remove_tree(t[0], t[1]);
				wood_amount_[u.info.player] += tree_wood_amount;
				u.delayed_chop = std::nullopt;
				sound::chop();
			}
		}
		else if (u.delayed_build)
		{
			auto t = u.delayed_build->first;

			if (rotate_to_tile(t))
			{
				ramps_.add_ramp(t, u.delayed_build->second);
				wood_amount_[u.info.player] -= ramp_cost(u.delayed_build->second);
				u.delayed_build = std::nullopt;
				sound::build();
			}
		}
	}

	for (auto id : kill)
		units_.erase(id);

	std::vector<arrow_data> alive_arrows;

	for (auto & a : arrows_)
	{
		a.time += dt;
		if (a.time < a.total_time)
			alive_arrows.push_back(a);
		else
		{
			if (auto id = unit_at(a.target))
			{
				auto & u = units_.at(*id);
				u.info.health -= arrow_damage;
				u.damage_animation = 1.f;
				sound::arrow_hit();
			}
		}
	}

	arrows_ = std::move(alive_arrows);
}

bool units::has_animation() const
{
	if (!arrows_.empty())
		return true;

	for (auto const & p : units_)
	{
		if (!p.second.movement_queue.empty())
			return true;
		if (p.second.delayed_arrow)
			return true;
		if (p.second.delayed_melee)
			return true;
		if (p.second.delayed_chop)
			return true;
		if (p.second.delayed_build)
			return true;
	}

	return false;
}
