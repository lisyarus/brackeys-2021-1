#include <game/battle_scene.hpp>

#include <game/settings.hpp>
#include <game/render.hpp>
#include <game/map.hpp>
#include <game/selection.hpp>
#include <game/terrain.hpp>
#include <game/trees.hpp>
#include <game/ramps.hpp>
#include <game/units.hpp>
#include <game/path_finder.hpp>
#include <game/player.hpp>
#include <game/button.hpp>
#include <game/sound.hpp>

#include <psemek/gfx/painter.hpp>

#include <psemek/geom/camera.hpp>
#include <psemek/geom/interval.hpp>
#include <psemek/geom/ray.hpp>
#include <psemek/geom/intersection.hpp>
#include <psemek/geom/homogeneous.hpp>
#include <psemek/geom/swizzle.hpp>

#include <psemek/util/clock.hpp>
#include <psemek/util/to_string.hpp>

#include <psemek/random/device.hpp>
#include <psemek/random/generator.hpp>
#include <psemek/random/uniform.hpp>

#include <psemek/log/log.hpp>

#include <buttons/skip_ppm.hpp>
#include <buttons/shoot_ppm.hpp>
#include <buttons/melee_ppm.hpp>
#include <buttons/move_ppm.hpp>
#include <buttons/build_ppm.hpp>
#include <buttons/chop_ppm.hpp>
#include <buttons/wood_ppm.hpp>

namespace
{

struct battle_scene
	: app::scene_base
{
	battle_scene(map m, std::function<void()> on_quit, player_controller_factory player0, player_controller_factory player1);

	void on_resize(int width, int height) override;
	void on_mouse_move(int x, int y, int dx, int dy) override;
	void on_mouse_wheel(int delta) override;
	void on_left_button_down() override;
	void on_left_button_up() override;
	void on_right_button_down() override;
	void on_key_down(SDL_Keycode key) override;

	void update() override;
	void present() override;

private:
	map map_;
	terrain terrain_;
	selection selection_;
	trees trees_;
	ramps ramps_;
	units units_;
	path_finder path_finder_;

	random::generator rng_{random::device{}};

	util::clock<std::chrono::duration<float>> global_clock_;
	util::clock<std::chrono::duration<float>> update_clock_;

	bool mouse_over_button_ = false;

	geom::point<float, 3> camera_target_;
	float camera_distance_;
	geom::spherical_camera camera_;

	std::optional<geom::point<int, 2>> selected_tile_;
	std::optional<units::id> followed_unit_;
	tile current_ramp_type_ = flat{};

	game_state state_ = states::start_game{};
	bool last_turn_skipped_ = false;
	int current_player_ = 0;

	enum class ui_action_mode
	{
		move,
		shoot,
		melee,
		chop,
		build,
	};
	ui_action_mode action_mode_;

	std::unique_ptr<player_controller> controllers_[2];

	gfx::painter painter_;

	button exit_button_;

	button move_button_;
	button shoot_button_;
	button melee_button_;
	button build_button_;
	button chop_button_;
	button end_turn_button_;

	std::vector<button *> action_buttons_;

	gfx::texture_2d wood_icon_;
};

battle_scene::battle_scene(map m, std::function<void()> on_quit, player_controller_factory player0, player_controller_factory player1)
	: map_{std::move(m)}
	, terrain_{map_}
	, trees_{map_}
	, ramps_{map_}
	, units_{map_, ramps_, trees_}
	, path_finder_{map_, trees_, ramps_, units_}
	, exit_button_("Exit", 160, 64)
	, move_button_(load_texture(buttons::move_ppm, false), 2, "Move")
	, shoot_button_(load_texture(buttons::shoot_ppm, false), 2, "Shoot")
	, melee_button_(load_texture(buttons::melee_ppm, false), 2, "Melee attack")
	, build_button_(load_texture(buttons::build_ppm, false), 2, "Build")
	, chop_button_(load_texture(buttons::chop_ppm, false), 2, "Chop")
	, end_turn_button_(load_texture(buttons::skip_ppm, false), 2, "End turn")
	, wood_icon_(load_transparent_texture(buttons::wood_ppm, {255, 255, 255}))
{
	camera_.near_clip = 1.f;
	camera_.far_clip = 500.f;
	camera_.fov_y = geom::rad(60.f);
	camera_.distance = 300.f;
	camera_.target = {map_.size[0] / 2.f, map_.size[1] / 2.f, 0.f};
	camera_.elevation_angle = geom::rad(45.f);
	camera_.azimuthal_angle = geom::rad(0.f);

	camera_target_ = camera_.target;
	camera_distance_ = 50.f;

	controllers_[0] = player0(units_, path_finder_);
	controllers_[1] = player1(units_, path_finder_);

	exit_button_.set_on_click(std::move(on_quit));
	end_turn_button_.set_on_click([this]{
		controllers_[current_player_]->on_human_action(actions::skip{});
	});

	move_button_.set_on_click([this]{
		action_mode_ = ui_action_mode::move;
	});

	shoot_button_.set_on_click([this]{
		action_mode_ = ui_action_mode::shoot;
	});

	melee_button_.set_on_click([this]{
		action_mode_ = ui_action_mode::melee;
	});

	chop_button_.set_on_click([this]{
		action_mode_ = ui_action_mode::chop;
	});

	build_button_.set_on_click([this]{
		action_mode_ = ui_action_mode::build;
	});

	action_buttons_ = {
		&end_turn_button_,
		&build_button_,
		&chop_button_,
		&move_button_,
		&shoot_button_,
		&melee_button_,
	};
}

void battle_scene::on_resize(int width, int height)
{
	scene_base::on_resize(width, height);
	gl::Viewport(0, 0, width, height);
	camera_.set_fov(camera_.fov_y, (1.f * width) / height);

	exit_button_.set_position({exit_button_.width() / 2 + 50, height - 50 - exit_button_.height() / 2});
}

void battle_scene::on_mouse_move(int x, int y, int dx, int dy)
{
	scene_base::on_mouse_move(x, y, dx, dy);

	if ((is_middle_button_down() || (is_key_down(SDLK_LSHIFT) && is_right_button_down())))
	{
		camera_.elevation_angle += dy * 0.01f * (global_settings().inverted_y ? -1.f : 1.f) * global_settings().mouse_sensitivity;
		camera_.azimuthal_angle -= dx * 0.01f * global_settings().mouse_sensitivity;

		camera_.elevation_angle = geom::clamp(camera_.elevation_angle, {geom::rad(30.f), geom::rad(75.f)});
	}

	mouse_over_button_ = false;

	mouse_over_button_ |= exit_button_.on_mouse_move({x, y});
	for (auto b : action_buttons_)
		mouse_over_button_ |= b->on_mouse_move({x, y});
}

void battle_scene::on_mouse_wheel(int delta)
{
	scene_base::on_mouse_wheel(delta);

	camera_distance_ *= std::pow(0.8f, delta * (global_settings().inverted_wheel ? -1.f : 1.f));
	camera_distance_ = geom::clamp(camera_distance_, {5.f, 100.f});
}

void battle_scene::on_left_button_down()
{
	scene_base::on_left_button_down();

	if (exit_button_.on_mouse_down()) return;
	for (auto b : action_buttons_)
		if (b->on_mouse_down()) return;

	if (selected_tile_)
	{
		auto ctrl = controllers_[current_player_].get();

		auto t = *selected_tile_;
		auto id = units_.unit_at(t);

		if (std::get_if<states::select>(&state_))
		{
			if (id && units_.get(*id).player == current_player_)
				ctrl->on_human_action(actions::select{*id});
		}
		else if (auto st = std::get_if<states::move>(&state_))
		{
			auto const & u = units_.get(st->selected_unit);
			auto v = units_.unit_at(t);

			if (v && units_.get(*v).player == current_player_ && st->moves == max_moves(u.type))
				ctrl->on_human_action(actions::select{*v});
			else if (st->reachable_tiles.count(t) && st->reachable_tiles[t].cost > 0 && action_mode_ == ui_action_mode::move)
				ctrl->on_human_action(actions::move{t});
			else if (st->shootable_tiles.count(t) && v && (units_.get(*v).player != current_player_) && (action_mode_ == ui_action_mode::move || action_mode_ == ui_action_mode::shoot) && (u.type == unit_type::archer))
				ctrl->on_human_action(actions::shoot{t});
			else if (st->melee_tiles.count(t) && v && (units_.get(*v).player != current_player_) && (action_mode_ == ui_action_mode::move || action_mode_ == ui_action_mode::melee) && is_melee(u.type))
				ctrl->on_human_action(actions::melee{st->melee_tiles[t].position, t});
			else if (st->choppable_tiles.count(t) && (action_mode_ == ui_action_mode::move || action_mode_ == ui_action_mode::chop))
				ctrl->on_human_action(actions::chop{t});
			else if (st->buildable_tiles.count(t) && (action_mode_ == ui_action_mode::build))
			{
				bool can_build = units_.wood_amount(current_player_) >= ramp_cost(current_ramp_type_);
				if (std::get_if<slope>(&current_ramp_type_))
				{
					auto g = *ramps_.effective_geometry(t);
					can_build &= static_cast<bool>(std::get_if<flat>(&g));
				}

				if (can_build)
					ctrl->on_human_action(actions::build{t, current_ramp_type_});
			}
		}
	}
}

void battle_scene::on_left_button_up()
{
	scene_base::on_left_button_up();

	exit_button_.on_mouse_up();
	for (auto b : action_buttons_)
		b->on_mouse_up();
}

void battle_scene::on_right_button_down()
{
	scene_base::on_right_button_down();

	if (selected_tile_ && !is_key_down(SDLK_LSHIFT) && (!global_settings().auto_camera || controllers_[current_player_]->free_camera()))
	{
		auto const t = *selected_tile_;
		auto const c = map_.tile_center_height(t[0], t[1]);

		if (c)
			camera_target_ = {t[0] + 0.5f, t[1] + 0.5f, *c};
	}
}

void battle_scene::on_key_down(SDL_Keycode key)
{
	scene_base::on_key_down(key);

	if (key == SDLK_r && std::get_if<states::move>(&state_) && action_mode_ == ui_action_mode::build)
	{
		if (std::get_if<flat>(&current_ramp_type_))
		{
			current_ramp_type_ = slope{0};
		}
		else if (auto s = std::get_if<slope>(&current_ramp_type_))
		{
			if (s->direction < 3)
				current_ramp_type_ = slope{s->direction + 1};
			else
				current_ramp_type_ = flat{};
		}
	}
}

void battle_scene::update()
{
	float const dt = update_clock_.restart().count();

	selected_tile_ = std::nullopt;

	auto make_move_state = [this](units::id id, int moves)
	{
		auto t = units_.get(id).tile;
		states::move result{id, moves, path_finder_.reachable(t, moves), path_finder_.shootable(t), {}, {}, {}};
		result.melee_tiles = path_finder_.melee_attackable(result.reachable_tiles, moves);
		result.choppable_tiles = path_finder_.choppable(result.reachable_tiles, moves);
		result.buildable_tiles = path_finder_.buildable(result.reachable_tiles, moves);
		return result;
	};

	if (mouse() && !mouse_over_button_)
	{
		auto const m = *mouse();
		auto const position = camera_.position();
		auto const direction = geom::normalized(camera_.direction((2.f * m[0]) / width() - 1.f, 1.f - (2.f * m[1]) / height()));

		geom::ray ray{position, direction};

		float t = std::numeric_limits<float>::infinity();

		for (int y = 0; y < map_.size[1]; ++y)
		{
			for (int x = 0; x < map_.size[0]; ++x)
			{
				std::optional<std::array<geom::point<float, 3>, 4>> points;

				if (ramps_.has_ramp(x, y))
					points = ramps_.ramp_surface(x, y);
				else
					points = map_.tile_surface(x, y);

				if (!points) return;

				geom::simplex tri0{(*points)[0], (*points)[1], (*points)[2]};
				geom::simplex tri1{(*points)[2], (*points)[1], (*points)[3]};

				auto i0 = geom::intersection(ray, tri0);
				auto i1 = geom::intersection(ray, tri1);

				if (i0 && *i0 < t)
				{
					t = *i0;
					selected_tile_ = {x, y};
				}

				if (i1 && *i1 < t)
				{
					t = *i1;
					selected_tile_ = {x, y};
				}
			}
		}
	}

	auto force_skip = [&](std::string_view message)
	{
		log::warning() << "Player " << (current_player_ + 1) << " " << controllers_[current_player_]->name() << ": " << message;

		if (last_turn_skipped_)
			state_ = states::draw{};
		else
		{
			state_ = states::end_turn{};
			last_turn_skipped_ = true;
		}
	};

	if (auto st = std::get_if<states::start_game>(&state_))
	{
		st->cooldown -= dt;
		if (st->cooldown <= 0.f)
		{
			action_mode_ = ui_action_mode::move;
			state_ = states::select{};
		}
	}
	else if (std::get_if<states::select>(&state_))
	{
		auto act = controllers_[current_player_]->action();
		if (act)
		{
			if (auto a = std::get_if<actions::select>(&(*act)))
			{
				last_turn_skipped_ = false;
				auto const & u = units_.get(a->id);
				if (u.player == current_player_)
				{
					state_ = make_move_state(a->id, max_moves(u.type));
					action_mode_ = ui_action_mode::move;
					sound::select();

					if (!controllers_[current_player_]->free_camera())
						followed_unit_ = a->id;
				}
				else force_skip("Can't select enemy unit");
			}
			else if (std::get_if<actions::skip>(&(*act)))
			{
				state_ = states::end_turn{};
				last_turn_skipped_ = true;
			}
			else force_skip("Invalid action in select mode");
		}
	}
	else if (auto st = std::get_if<states::move>(&state_))
	{
		auto const & u = units_.get(st->selected_unit);

		auto act = controllers_[current_player_]->action();
		if (act)
		{
			if (auto a = std::get_if<actions::select>(&(*act)))
			{
				if (st->moves == max_moves(u.type))
				{
					if (units_.get(a->id).player == current_player_)
					{
						state_ = make_move_state(a->id, max_moves(units_.get(a->id).type));
						action_mode_ = ui_action_mode::move;
						sound::select();

						if (!controllers_[current_player_]->free_camera())
							followed_unit_ = a->id;
					}
					else force_skip("Can't select enemy unit");
				}
				else force_skip("Can't reselect unit after move");
			}
			else if (auto a = std::get_if<actions::move>(&(*act)))
			{
				if (st->reachable_tiles.count(a->tile))
				{
					int cost = st->reachable_tiles.at(a->tile).cost;
					if (cost != 0 && cost <= st->moves)
					{
						int new_moves = st->moves - cost;
						std::vector<int> path;
						auto p = a->tile;
						while (p != u.tile)
						{
							path.push_back(st->reachable_tiles.at(p).direction);
							p -= directions[path.back()];
						}
						for (auto it = path.rbegin(); it != path.rend(); ++it)
							units_.move(st->selected_unit, *it);
						if (new_moves == 0)
						{
							state_ = states::end_turn{};
						}
						else
						{
							state_ = make_move_state(st->selected_unit, new_moves);
						}
					}
					else force_skip(util::to_string("Tile too far away (cost = ", cost, ")"));
				}
				else force_skip("Unreachable tile");
			}
			else if (auto a = std::get_if<actions::shoot>(&(*act)))
			{
				if (units_.get(st->selected_unit).type == unit_type::archer && st->shootable_tiles.count(a->target))
				{
					auto id = units_.unit_at(a->target);
					if (id)
					{
						auto const & v = units_.get(*id);
						if (v.player != current_player_)
						{
							auto ids = units_.unit_group(st->selected_unit);
							for (auto id : ids)
							{
								if (path_finder_.shootable(units_.get(id).tile, v.tile))
									units_.fire(id, v.tile);
							}
							state_ = states::end_turn{};
						}
						else force_skip("Can't shoot friendly unit");
					}
					else force_skip("Shooting target has no unit");
				}
				else force_skip("Wrong unit type or unreachable tile for shooting");
			}
			else if (auto a = std::get_if<actions::melee>(&(*act)))
			{
				if (is_melee(units_.get(st->selected_unit).type) && st->melee_tiles.count(a->target))
				{
					auto id = units_.unit_at(a->target);
					if (id)
					{
						auto const & v = units_.get(*id);
						if (v.player != current_player_)
						{
							int cost = st->melee_tiles.at(a->target).cost;
							if (cost != 0 && cost <= st->moves)
							{
								std::vector<int> path;
								auto p = a->position;
								while (p != u.tile)
								{
									path.push_back(st->reachable_tiles.at(p).direction);
									p -= directions[path.back()];
								}
								for (auto it = path.rbegin(); it != path.rend(); ++it)
									units_.move(st->selected_unit, *it);

								auto ids = units_.unit_group(st->selected_unit);
								for (auto id : ids)
								{
									if (id == st->selected_unit)
									{
										units_.melee(st->selected_unit, a->target);
									}
									else
									{
										auto const & u = units_.get(id);
										path_finder::reachable_map reach;
										reach[u.tile] = {0, 0};

										auto attackable = path_finder_.melee_attackable(reach, 1);

										std::vector<geom::point<int, 2>> targets;

										for (auto const & t : attackable)
										{
											if (auto id = units_.unit_at(t.first))
											{
												if (units_.get(*id).player == 1 - current_player_)
													targets.push_back(t.first);
											}
										}

										if (!targets.empty())
										{
											auto i = random::uniform_distribution<int>(0, targets.size() - 1)(rng_);
											units_.melee(id, targets[i]);
										}
									}
								}
								state_ = states::end_turn{};
							}
							else force_skip("Wrong tile for melee");
						}
						else force_skip("Can't melee attack friendly unit");
					}
					else force_skip("Melee target has no unit");
				}
				else force_skip("Wrong unit type or wrong tile for melee");
			}
			else if (auto a = std::get_if<actions::chop>(&(*act)))
			{
				if (st->choppable_tiles.count(a->target))
				{
					int cost = st->choppable_tiles.at(a->target).cost;
					if (cost != 0 && cost <= st->moves)
					{
						std::vector<int> path;
						auto p = a->target - directions[st->choppable_tiles.at(a->target).direction];
						while (p != u.tile)
						{
							path.push_back(st->reachable_tiles.at(p).direction);
							p -= directions[path.back()];
						}
						for (auto it = path.rbegin(); it != path.rend(); ++it)
							units_.move(st->selected_unit, *it);
						units_.chop(st->selected_unit, st->choppable_tiles.at(a->target).direction);
						state_ = states::end_turn{};
					}
					else force_skip("Unreachanle tile for chop");
				}
				else force_skip("Unreachanle tile for chop");
			}
			else if (auto a = std::get_if<actions::build>(&(*act)))
			{
				if (st->buildable_tiles.count(a->target) && units_.wood_amount(current_player_) >= ramp_cost(a->type))
				{
					int cost = st->buildable_tiles.at(a->target).cost;
					if (cost != 0 && cost <= st->moves)
					{
						bool can_build = true;
						if (std::get_if<slope>(&current_ramp_type_))
						{
							auto g = *ramps_.effective_geometry(a->target);
							can_build &= static_cast<bool>(std::get_if<flat>(&g));
						}

						if (can_build)
						{
							std::vector<int> path;
							auto p = a->target - directions[st->buildable_tiles.at(a->target).direction];
							while (p != u.tile)
							{
								path.push_back(st->reachable_tiles.at(p).direction);
								p -= directions[path.back()];
							}
							for (auto it = path.rbegin(); it != path.rend(); ++it)
								units_.move(st->selected_unit, *it);
							units_.build(st->selected_unit, st->buildable_tiles.at(a->target).direction, a->type);
							state_ = states::end_turn{};
						}
						else force_skip("Wrong ramp type or surface type for build");
					}
					else force_skip("Unreachable tile for build");
				}
				else force_skip("Unreachable tile for build or insufficient resources");
			}
			else if (std::get_if<actions::skip>(&(*act)))
			{
				state_ = states::end_turn{};
			}
			else force_skip("Invalid action in move mode");
		}
	}
	else if (auto st = std::get_if<states::end_turn>(&state_))
	{
		if (!units_.has_animation() && !trees_.has_animation() && !ramps_.has_animation())
		{
			st->cooldown -= dt;
			if (st->cooldown <= 0.f)
			{
				state_ = states::select{};
				current_player_ = 1 - current_player_;
				followed_unit_ = std::nullopt;
				action_mode_ = ui_action_mode::move;

				auto ids = units_.player_units(current_player_);
				if (!ids.empty())
				{
					auto o = geom::point<float, 3>::zero();

					auto m = geom::vector<float, 3>::zero();
					for (auto id : ids)
						m += (units_.get_unit_position(id) - o) / (1.f * ids.size());

					if (global_settings().auto_camera)
						camera_target_ = o + m;
				}
				else
				{
					current_player_ = 1 - current_player_;
					state_ = states::victory{};
				}
			}
		}
	}

	controllers_[current_player_]->set_game_state(state_);

	if (std::get_if<states::start_game>(&state_))
	{
		for (auto b : action_buttons_)
			b->set_active(false);
	}
	else if (std::get_if<states::select>(&state_))
	{
		for (auto b : action_buttons_)
			b->set_active(b == &end_turn_button_);
		end_turn_button_.set_text("Skip turn");
	}
	else if (auto st = std::get_if<states::move>(&state_))
	{
		auto const & u = units_.get(st->selected_unit);
		end_turn_button_.set_active(true);
		end_turn_button_.set_text("End turn");
		build_button_.set_active(action_mode_ != ui_action_mode::build);
		chop_button_.set_active(action_mode_ != ui_action_mode::chop);
		shoot_button_.set_active((action_mode_ != ui_action_mode::shoot) && (u.type == unit_type::archer));
		melee_button_.set_active((action_mode_ != ui_action_mode::melee) && is_melee(u.type));
		move_button_.set_active(action_mode_ != ui_action_mode::move);
	}
	else if (std::get_if<states::end_turn>(&state_))
	{
		for (auto b : action_buttons_)
			b->set_active(false);
	}
	else if (std::get_if<states::victory>(&state_))
	{
		for (auto b : action_buttons_)
			b->set_active(false);
	}
	else if (std::get_if<states::draw>(&state_))
	{
		for (auto b : action_buttons_)
			b->set_active(false);
	}

	int action_button_x = width() - 50;
	for (auto b : action_buttons_)
	{
		if (!b->active()) continue;

		b->set_position({action_button_x - b->width() / 2, height() - 50 - b->height() / 2});
		action_button_x -= b->width();
		action_button_x -= 20;

		if (mouse())
			b->on_mouse_move(*mouse());
	}

	units_.update(dt);
	trees_.update(dt);
	ramps_.update(dt);

	if (followed_unit_ && global_settings().auto_camera)
		camera_target_ = units_.get_unit_position(*followed_unit_);

	camera_.target += (camera_target_ - camera_.target) * std::min(0.9f, dt * 10.f);

	float distance_update_speed = 20.f;
	if (std::get_if<states::start_game>(&state_))
		distance_update_speed = 7.f;
	camera_.distance += (camera_distance_ - camera_.distance) * std::min(0.9f, dt * distance_update_speed);
}

void battle_scene::present()
{
	std::vector<std::string> debug_text;

	gl::DepthMask(gl::TRUE);

	gl::ClearColor(0.8f, 0.8f, 1.f, 1.f);
	gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

	render_options options;
	options.time = global_clock_.count();
	options.camera_transform = camera_.transform();
	options.camera_pos = camera_.position();
	options.camera_dir = camera_.direction();
	options.light_dir = light_dir();

	gl::Enable(gl::DEPTH_TEST);
	gl::DepthFunc(gl::LEQUAL);
	gl::DepthMask(gl::TRUE);
	gl::Enable(gl::CULL_FACE);
	gl::Disable(gl::BLEND);

	terrain_.render(options);
	trees_.render(options);
	ramps_.render(options);
	units_.render(options);

	gl::Disable(gl::CULL_FACE);
	gl::DepthMask(gl::FALSE);
	gl::Enable(gl::BLEND);
	gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);

	std::vector<selection::tile> selected_tiles;

	bool const build_ramp_type_slope = static_cast<bool>(std::get_if<slope>(&current_ramp_type_));

	bool can_reselect = false;
	if (auto st = std::get_if<states::move>(&state_))
	{
		can_reselect = st->moves == max_moves(units_.get(st->selected_unit).type);
	}

	if (std::get_if<states::select>(&state_) || can_reselect)
	{
		std::optional<units::id> selected;
		if (auto st = std::get_if<states::move>(&state_))
			selected = st->selected_unit;

		auto ids = units_.player_units(current_player_);

		for (auto id : ids)
		{
			if (selected && id == *selected) continue;
			auto const & info = units_.get(id);
			auto points = ramps_.effective_surface(info.tile);
			if (points)
			{
				gfx::color_4f color = {1.f, 1.f, 1.f, 0.5f};
				if (controllers_[current_player_]->show_ui() && selected_tile_ && *selected_tile_ == info.tile)
				{
					color = player_colors[current_player_];
					color[3] = 0.5f;
				}
				selected_tiles.push_back({*points, 1.f, color});
			}
		}
	}

	if (auto st = std::get_if<states::move>(&state_))
	{
		auto const & info = units_.get(st->selected_unit);
		auto points = ramps_.effective_surface(info.tile);
		if (points)
		{
			gfx::color_4f color = player_colors[current_player_];
			color[3] = 0.5f;
			selected_tiles.push_back({*points, 1.f, color});
		}

		if (action_mode_ == ui_action_mode::move)
		{
			for (auto const & p : st->reachable_tiles)
			{
				if (p.second.cost == 0) continue;

				auto points = ramps_.effective_surface(p.first);
				gfx::color_4f color{1.f, 1.f, 1.f, 0.5f};
				if (controllers_[current_player_]->show_ui() && selected_tile_ && p.first == *selected_tile_)
					color = {1.f, 1.f, 1.f, 0.75f};
				if (points)
					selected_tiles.push_back({*points, 0.f, color});
			}
		}

		if (action_mode_ == ui_action_mode::shoot)
		{
			for (auto const & t : st->shootable_tiles)
			{
				auto points = ramps_.effective_surface(t);
				gfx::color_4f color{1.f, 1.f, 1.f, 0.5f};
				if (points)
					selected_tiles.push_back({*points, 0.f, color});
			}
		}

		if (((action_mode_ == ui_action_mode::move) || (action_mode_ == ui_action_mode::shoot)) && (units_.get(st->selected_unit).type == unit_type::archer))
		{
			for (auto id : units_.player_units(1 - current_player_))
			{
				auto const & u = units_.get(id);

				if (st->shootable_tiles.count(u.tile) > 0)
				{
					auto points = ramps_.effective_surface(u.tile);
					gfx::color_4f color{1.f, 1.f, 1.f, 0.5f};
					if (controllers_[current_player_]->show_ui() && selected_tile_ && u.tile == *selected_tile_)
					{
						color = player_colors[1 - current_player_];
						color[3] = 0.5f;
					}
					if (points)
						selected_tiles.push_back({*points, 1.f, color});
				}
			}
		}

		if (action_mode_ == ui_action_mode::melee)
		{
			for (auto const & t : st->melee_tiles)
			{
				auto points = ramps_.effective_surface(t.first);
				gfx::color_4f color{1.f, 1.f, 1.f, 0.5f};
				if (points)
					selected_tiles.push_back({*points, 0.f, color});
			}
		}

		if (((action_mode_ == ui_action_mode::move) || (action_mode_ == ui_action_mode::melee)) && is_melee(units_.get(st->selected_unit).type))
		{
			for (auto id : units_.player_units(1 - current_player_))
			{
				auto const & u = units_.get(id);

				if (st->melee_tiles.count(u.tile) > 0)
				{
					auto points = ramps_.effective_surface(u.tile);
					gfx::color_4f color{1.f, 1.f, 1.f, 0.5f};
					bool show_position = false;
					if (controllers_[current_player_]->show_ui() && selected_tile_ && u.tile == *selected_tile_)
					{
						color = player_colors[1 - current_player_];
						color[3] = 0.5f;
						show_position = true;
					}
					if (points)
						selected_tiles.push_back({*points, 1.f, color});

					if (show_position)
					{
						points = ramps_.effective_surface(st->melee_tiles[u.tile].position);
						auto color = player_colors[current_player_];
						color[3] = 0.5f;
						if (points)
							selected_tiles.push_back({*points, 1.f, color});
					}
				}
			}
		}

		if ((action_mode_ == ui_action_mode::move) || (action_mode_ == ui_action_mode::chop))
		{
			for (auto const & p : st->choppable_tiles)
			{
				auto points = ramps_.effective_surface(p.first);
				gfx::color_4f color{1.f, 1.f, 1.f, 0.5f};
				if (controllers_[current_player_]->show_ui() && selected_tile_ && p.first == *selected_tile_)
					color = {0.f, 0.5f, 0.f, 0.5f};
				if (points)
					selected_tiles.push_back({*points, 1.f, color});
			}
		}

		if (action_mode_ == ui_action_mode::build)
		{
			for (auto const & p : st->buildable_tiles)
			{
				if (build_ramp_type_slope)
				{
					auto g = *ramps_.effective_geometry(p.first);
					if (std::get_if<slope>(&g)) continue;
				}

				auto points = ramps_.effective_surface(p.first);
				gfx::color_4f color{1.f, 1.f, 1.f, 0.5f};
				if (points)
					selected_tiles.push_back({*points, 0.f, color});
			}
		}
	}

	selection_.render(options, selected_tiles);

	std::optional<bool> show_build_cost;

	if (auto st = std::get_if<states::move>(&state_))
	{
		if (action_mode_ == ui_action_mode::build && selected_tile_ && st->buildable_tiles.count(*selected_tile_))
		{
			auto t = *selected_tile_;
			bool can_build = true;
			if (build_ramp_type_slope)
			{
				auto g = *ramps_.effective_geometry(t);
				can_build &= static_cast<bool>(std::get_if<flat>(&g));
			}

			if (can_build)
			{
				gfx::color_4f color{0.f, 1.f, 0.f, 0.5f};
				show_build_cost = true;
				if (ramp_cost(current_ramp_type_) > units_.wood_amount(current_player_))
				{
					color = {1.f, 0.f, 0.f, 0.5f};
					show_build_cost = false;
				}
				ramps_.render_ghost(options, *selected_tile_, current_ramp_type_, color);
			}
		}
	}

	for (auto id : units_.all_units())
	{
		auto const & u = units_.get(id);
		auto position = units_.get_unit_position(id);
		position[2] += 2.f;

		auto clip = options.camera_transform * geom::homogeneous(position);
		if (clip[3] <= 0.f) continue;

		auto screen = geom::swizzle<0, 1>(clip) / clip[3];
		screen[0] = (0.5f + screen[0] * 0.5f) * width();
		screen[1] = (0.5f - screen[1] * 0.5f) * height();

		float w = 10.f * max_health(u.type) / 100.f;
		float h = 4.f;
		float d = 1.f;

		float health = (1.f * u.health) / max_health(u.type);

		if (health > 0.f)
		{
			painter_.rect({{{screen[0] - w, screen[0] + w}, {screen[1] - h, screen[1] + h}}}, {0, 0, 0, 127});
			painter_.rect({{{screen[0] - w + d, screen[0] - w + d + 2.f * (w - d) * health}, {screen[1] - h + d, screen[1] + h - d}}}, {0, 192, 0, 127});
		}
	}

	if (show_build_cost && mouse())
	{
		auto m = *mouse() + geom::vector{10, 10};

		int w = wood_icon_.width();
		int h = wood_icon_.height();

		gfx::color_rgba color{255, 255, 255, 0};
		if (!(*show_build_cost))
			color = {255, 0, 0, 127};

		painter_.texture(wood_icon_, {{{m[0], m[0] + w}, {m[1], m[1] + h}}}, color);

		gfx::painter::text_options opts;
		opts.scale = 2.f;
		opts.x = gfx::painter::x_align::left;
		opts.y = gfx::painter::y_align::center;

		auto str = util::to_string(ramp_cost(current_ramp_type_));
		geom::point<float, 2> p{m[0] + w, m[1] + h / 2};

		opts.c = {0, 0, 0, 255};
		painter_.text(p + geom::vector{1.f, 1.f}, str, opts);

		opts.c = color;
		opts.c[3] = 255;
		painter_.text(p, str, opts);
	}

	int status_text_line = 0;
	auto put_status_text = [&](std::string const & text, gfx::color_rgba const & color)
	{
		gfx::painter::text_options opts;
		opts.scale = 4.f;

		opts.c = {0, 0, 0, 255};
		painter_.text({width() / 2.f + 2.f, 50.f + status_text_line * 48.f + 2.f}, text, opts);
		opts.c = color;
		painter_.text({width() / 2.f, 50.f + status_text_line * 48.f}, text, opts);

		++status_text_line;
	};

	if (!std::get_if<states::start_game>(&state_) && !std::get_if<states::draw>(&state_))
	{
		put_status_text(util::to_string("Player ", current_player_ + 1), gfx::to_coloru8(gfx::light(player_colors[current_player_], 0.25f)));

		if (std::get_if<states::select>(&state_))
			put_status_text("Select unit", {255, 255, 255, 255});
		else if (std::get_if<states::move>(&state_))
		{
			if (action_mode_ == ui_action_mode::move)
				put_status_text("Select tile to move", {255, 255, 255, 255});
			else if (action_mode_ == ui_action_mode::shoot)
				put_status_text("Select target", {255, 255, 255, 255});
			else if (action_mode_ == ui_action_mode::melee)
				put_status_text("Select target", {255, 255, 255, 255});
			else if (action_mode_ == ui_action_mode::chop)
				put_status_text("Select tree", {255, 255, 255, 255});
			else if (action_mode_ == ui_action_mode::build)
			{
				put_status_text("Select tile to build", {255, 255, 255, 255});
				put_status_text("Press [R] to cycle ramp types", {255, 255, 255, 255});
			}
		}
		if (std::get_if<states::victory>(&state_))
			put_status_text("Victory!", {255, 255, 0, 255});
	}

	if (std::get_if<states::draw>(&state_))
	{
		put_status_text("Draw!", {255, 255, 0, 255});
	}

	for (int i = 0; i < debug_text.size(); ++i)
	{
		gfx::painter::text_options opts;
		opts.scale = 2;
		opts.x = gfx::painter::x_align::left;
		opts.y = gfx::painter::y_align::top;

		painter_.text({20.f, 20.f + i * 24.f}, debug_text[i], opts);
	}

	exit_button_.render(painter_);
	if (controllers_[current_player_]->show_ui())
	{
		for (auto b : action_buttons_)
			if (b->active())
				b->render(painter_);
	}

	if (std::get_if<states::select>(&state_) || std::get_if<states::move>(&state_) || std::get_if<states::end_turn>(&state_))
	{
		int w = wood_icon_.width() * 2;
		int h = wood_icon_.height() * 2;
		painter_.texture(wood_icon_, {{{width() - w - 50, width() - 50}, {height() - 134 - h, height() - 134}}});

		gfx::painter::text_options opts;
		opts.scale = 3;
		opts.x = gfx::painter::x_align::right;
		opts.y = gfx::painter::y_align::center;

		geom::point<float, 2> p{width() - w - 50, height() - 134 - h / 2};
		auto str = util::to_string(units_.wood_amount(current_player_));

		opts.c = {0, 0, 0, 255};
		painter_.text(p + geom::vector{1.f, 1.f}, str, opts);

		opts.c = {255, 255, 255, 255};
		painter_.text(p, str, opts);
	}

	painter_.render(geom::window_camera{width(), height()}.transform());
}

}

std::unique_ptr<app::scene> make_battle_scene(map m, std::function<void()> on_quit, player_controller_factory player0, player_controller_factory player1)
{
	return std::make_unique<battle_scene>(std::move(m), std::move(on_quit), std::move(player0), std::move(player1));
}
