#include <game/main_menu_scene.hpp>
#include <game/settings_scene.hpp>
#include <game/map_gen_scene.hpp>
#include <game/howto_scene.hpp>
#include <game/battle_scene.hpp>
#include <game/button.hpp>
#include <game/player.hpp>

#include <psemek/gfx/gl.hpp>
#include <psemek/gfx/painter.hpp>

#include <psemek/geom/camera.hpp>

#include <psemek/random/device.hpp>
#include <psemek/random/generator.hpp>

#include <psemek/util/to_string.hpp>

#include <vector>

namespace
{

struct main_menu_scene
	: app::scene_base
{
	main_menu_scene(std::function<void()> on_quit, app::scene_manager & scene_manager);

	void on_resize(int width, int height) override;
	void on_mouse_move(int x, int y, int dx, int dy) override;
	void on_left_button_down() override;
	void on_left_button_up() override;

	void present() override;

private:
	std::function<void()> on_quit_;
	app::scene_manager & scene_manager_;

	std::vector<button> buttons_;

	gfx::painter painter_;
};

main_menu_scene::main_menu_scene(std::function<void()> on_quit, app::scene_manager & scene_manager)
	: on_quit_(std::move(on_quit))
	, scene_manager_(scene_manager)
{
	int const button_width = 320;
	int const button_height = 64;

	buttons_.emplace_back("New game", button_width, button_height);
	buttons_.back().set_on_click([this]{
		scene_manager_.push_scene(make_map_generation_scene([this]{
			scene_manager_.pop_scene();
		}, [this](std::unique_ptr<app::scene> scene){
			scene_manager_.pop_scene();
			scene_manager_.push_scene(std::move(scene));
		}));
	});

	buttons_.emplace_back("How to play", button_width, button_height);
	buttons_.back().set_on_click([this]{
		scene_manager_.push_scene(make_howto_scene([this]{
			scene_manager_.pop_scene();
		}));
	});

	buttons_.emplace_back("Settings", button_width, button_height);
	buttons_.back().set_on_click([this]{
		scene_manager_.push_scene(make_settings_scene([this]{
			scene_manager_.pop_scene();
		}));
	});

	buttons_.emplace_back("Quit", button_width, button_height);
	buttons_.back().set_on_click([this]{ on_quit_(); });
}

void main_menu_scene::on_resize(int width, int height)
{
	scene_base::on_resize(width, height);
	gl::Viewport(0, 0, width, height);

	int const button_margin = 20;

	int menu_height = 0;
	for (int i = 0; i < buttons_.size(); ++i)
	{
		menu_height += buttons_[i].height();
		if (i > 0)
			menu_height += button_margin;
	}

	int x = width / 2;
	int y = height / 2 - menu_height / 2;

	for (auto & b : buttons_)
	{
		b.set_position({x, y + b.height() / 2});
		y += b.height();
		y += button_margin;
	}
}

void main_menu_scene::on_mouse_move(int x, int y, int dx, int dy)
{
	scene_base::on_mouse_move(x, y, dx, dy);

	for (auto & b : buttons_)
		b.on_mouse_move({x, y});
}

void main_menu_scene::on_left_button_down()
{
	scene_base::on_left_button_down();

	for (auto & b : buttons_)
		if (b.on_mouse_down())
			return;
}

void main_menu_scene::on_left_button_up()
{
	scene_base::on_left_button_up();

	for (auto & b : buttons_)
		b.on_mouse_up();
}

void main_menu_scene::present()
{
	gl::ClearColor(0.8f, 0.8f, 1.f, 1.f);
	gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

	for (auto & b : buttons_)
	{
		b.render(painter_);
	}

	{
		std::string const str = "TO GATHER AND FIGHT";

		gfx::painter::text_options opts;
		opts.scale = 4.f;
		opts.x = gfx::painter::x_align::center;
		opts.y = gfx::painter::y_align::center;

		float x = width() / 2;
		float y = buttons_[0].position()[1] / 2;

		opts.c = {0, 0, 0, 255};
		painter_.text({x + 1.f, y + 1.f}, str, opts);

		opts.c = {0, 64, 192, 255};
		painter_.text({x, y}, str, opts);
	}

	{
		std::vector<std::string> bottom_text;
		bottom_text.push_back("by lisyarus (lisyarus.itch.io)");
		bottom_text.push_back("14 Feb 2021 - 21 Feb 2021");
		bottom_text.push_back("Created for Brackeys Game Jam 2021.1");

		gfx::painter::text_options opts;
		opts.scale = 2.f;

		for (int i = 0; i < bottom_text.size(); ++i)
		{
			float x = width() / 2.f;
			float y = height() - 20.f - i * 24.f;

			opts.c = {0, 0, 0, 255};
			painter_.text({x + 1.f, y + 1.f}, bottom_text[i], opts);

			opts.c = {255, 255, 0, 255};
			painter_.text({x, y}, bottom_text[i], opts);
		}
	}

	{
		std::string const str = "sound from zapsplat.com";

		gfx::painter::text_options opts;
		opts.scale = 2.f;
		opts.x = gfx::painter::x_align::right;
		opts.y = gfx::painter::y_align::bottom;

		float x = width() - 5;
		float y = height() - 5;

		opts.c = {0, 0, 0, 255};
		painter_.text({x + 1.f, y + 1.f}, str, opts);

		opts.c = {255, 255, 0, 255};
		painter_.text({x, y}, str, opts);
	}

	painter_.render(geom::window_camera{width(), height()}.transform());
}

}

std::unique_ptr<app::scene> make_main_menu_scene(std::function<void()> on_quit, app::scene_manager & scene_manager)
{
	return std::make_unique<main_menu_scene>(std::move(on_quit), scene_manager);
}
