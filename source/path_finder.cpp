#include <game/path_finder.hpp>

#include <psemek/geom/box.hpp>
#include <psemek/geom/simplex.hpp>
#include <psemek/geom/intersection.hpp>
#include <psemek/geom/swizzle.hpp>

path_finder::path_finder(map const & m, trees const & t, ramps const & r, units const & u)
	: map_{m}
	, trees_{t}
	, ramps_{r}
	, units_{u}
{}

bool path_finder::passable(geom::point<int, 2> const & tile, bool ignore_units) const
{
	if ((tile[0] < 0) || (tile[0] >= map_.size[0]) || (tile[1] < 0) || (tile[1] >= map_.size[1])) return false;

	auto g = map_.geometry(tile[0], tile[1]);
	return (map_.height(tile[0], tile[1]) > 0 || ramps_.has_ramp(tile[0], tile[1]) || static_cast<bool>(std::get_if<slope>(&g)))
		&& !trees_.has_tree(tile[0], tile[1]) && (ignore_units || !units_.unit_at(tile));
}

path_finder::reachable_map path_finder::reachable(geom::point<int, 2> const & start, int moves) const
{
	reachable_map result;

	result[start] = {0, 0};

	std::deque<geom::point<int, 2>> queue;
	queue.push_back(start);

	while (!queue.empty())
	{
		auto const t = queue.front();
		queue.pop_front();

		int const cost = result.at(t).cost;

		auto h0 = *ramps_.effective_height(t);
		auto g0 = *ramps_.effective_geometry(t);

		for (int dir = 0; dir < 4; ++dir)
		{
			auto q = t + directions[dir];
			int new_cost = cost + 1;
			if (new_cost > moves) continue;
			if (!passable(q)) continue;

			auto h1 = *ramps_.effective_height(q);
			auto g1 = *ramps_.effective_geometry(q);

			bool can_move = true;

			if (std::get_if<flat>(&g0))
			{
				if (std::get_if<flat>(&g1))
				{
					can_move &= (h0 == h1);
				}
				else if (auto s1 = std::get_if<slope>(&g1))
				{
					if (s1->direction == dir)
						can_move &= (h0 == h1);
					else if (s1->direction == ((dir + 2) % 4))
						can_move &= (h0 == h1 + 1);
					else
						can_move &= false;
				}
			}
			else if (auto s0 = std::get_if<slope>(&g0))
			{
				if (s0->direction == dir)
				{
					if (std::get_if<flat>(&g1))
					{
						can_move &= (h0 + 1 == h1);
					}
					else if (auto s1 = std::get_if<slope>(&g1))
					{
						can_move &= ((h0 + 1 == h1) && (s0->direction == s1->direction))
									|| ((h0 == h1) && (s0->direction == ((s1->direction + 2) % 4)));
					}
				}
				else if (s0->direction == ((dir + 2) % 4))
				{
					if (std::get_if<flat>(&g1))
					{
						can_move &= (h0 == h1);
					}
					else if (auto s1 = std::get_if<slope>(&g1))
					{
						can_move &= ((h0 == h1 + 1) && (s0->direction == s1->direction))
									|| ((h0 == h1) && (s0->direction == ((s1->direction + 2) % 4)));
					}
				}
				else
				{
					if (std::get_if<flat>(&g1))
					{
						can_move &= false;
					}
					else if (auto s1 = std::get_if<slope>(&g1))
					{
						can_move &= (h0 == h1) && (s0->direction == s1->direction);
					}
				}
			}

			if (!can_move) continue;

			if (result.count(q) == 0 || result[q].cost > new_cost)
			{
				result[q] = {new_cost, dir};
				queue.push_back(q);
			}
		}
	}

	return result;
}

bool path_finder::shootable(geom::point<int, 2> const & start, geom::point<int, 2> const & target) const
{
	geom::point<float, 3> s{start[0] + 0.5f, start[1] + 0.5f, 0.f};
	s[2] = ramps_.height_at(s[0], s[1]) + 0.8f;

	if (trees_.has_tree(target[0], target[1])) return false;
	if (*ramps_.effective_height(target) == 0) return false;

	geom::point<float, 3> e{target[0] + 0.5f, target[1] + 0.5f, 0.f};
	e[2] = ramps_.height_at(e[0], e[1]) + 0.8f;

	if (geom::length(geom::vector<float, 2>{start[0] - target[0], start[1] - target[1]}) > 16.f + 4.f * (s[2] - e[2])) return false;

	std::set<geom::point<int, 2>> ignored;
	ignored.insert(start);
	ignored.insert(target);

	return visible(s, e, ignored);
}

path_finder::tile_set path_finder::shootable(geom::point<int, 2> const & start) const
{
	geom::point<float, 3> s{start[0] + 0.5f, start[1] + 0.5f, 0.f};
	s[2] = ramps_.height_at(s[0], s[1]) + 0.8f;

	path_finder::tile_set result;
	for (int y = 0; y < map_.size[1]; ++y)
	{
		for (int x = 0; x < map_.size[0]; ++x)
		{
			if (trees_.has_tree(x, y)) continue;
			if (*ramps_.effective_height({x, y}) == 0) continue;

			geom::point<float, 3> e{x + 0.5f, y + 0.5f, 0.f};
			e[2] = ramps_.height_at(e[0], e[1]) + 0.8f;

			if (geom::length(geom::vector<float, 2>{start[0] - x, start[1] - y}) > 16.f + 4.f * (s[2] - e[2])) continue;

			std::set<geom::point<int, 2>> ignored;
			ignored.insert(start);
			ignored.insert(geom::point{x, y});

			if (visible(s, e, ignored))
				result.insert(geom::point{x, y});
		}
	}

	return result;
}

bool path_finder::visible(geom::point<float, 3> const & start, geom::point<float, 3> const & end) const
{
	return visible(start, end, {});
}

bool path_finder::visible(geom::point<float, 3> const & start, geom::point<float, 3> const & end, std::set<geom::point<int, 2>> const & ignored) const
{
	int minx = std::floor(std::min(start[0], end[0]));
	int maxx = std::ceil(std::max(start[0], end[0]));
	int miny = std::floor(std::min(start[1], end[1]));
	int maxy = std::ceil(std::max(start[1], end[1]));

	minx = std::max(minx, 0);
	miny = std::max(miny, 0);

	maxx = std::min(maxx, map_.size[0] - 1);
	maxy = std::min(maxy, map_.size[1] - 1);

	geom::simplex s2d{geom::swizzle<0, 1>(start), geom::swizzle<0, 1>(end)};

	for (int y = miny; y <= maxy; ++y)
	{
		for (int x = minx; x <= maxx; ++x)
		{
			if (ignored.count(geom::point{x, y}))
				continue;

			geom::box<float, 2> b{{{x, x + 1.f}, {y, y + 1.f}}};

			auto ion = geom::intersection(s2d, b);
			if (ion.empty()) continue;

			float z0 = start[2] + (end[2] - start[2]) * ion.min;
			float z1 = start[2] + (end[2] - start[2]) * ion.max;

			float z = std::min(z0, z1);

			float tz = ramps_.height_at(x + 0.5f, y + 0.5f);
			if (trees_.has_tree(x, y)) tz += 2.f;

			if (tz >= z) return false;

			if (units_.unit_at({x, y}))
			{
				b = geom::shrink(b, 0.15f);
				auto ion = geom::intersection(s2d, b);
				if (ion.empty()) continue;

				float z0 = start[2] + (end[2] - start[2]) * ion.min;
				float z1 = start[2] + (end[2] - start[2]) * ion.max;

				float z = std::min(z0, z1);

				if (tz + 1.f > z) return false;
			}
		}
	}

	return true;
}

path_finder::melee_map path_finder::melee_attackable(reachable_map const & reachable, int moves) const
{
	melee_map result;

	for (auto const & p : reachable)
	{
		int cost = p.second.cost + 1;
		if (cost > moves) continue;

		auto h0 = ramps_.effective_center_height(p.first);
		if (!h0) continue;

		for (int d = 0; d < 4; ++d)
		{
			auto t = p.first + directions[d];
			auto h = ramps_.effective_center_height(t);
			if (!h) continue;

			if (passable(t, true) && (std::abs(*h - *h0) < 0.75f))
			{
				if (result.count(t) == 0 || result[t].cost > cost)
				{
					result[t].cost = cost;
					result[t].position = p.first;
				}
			}
		}
	}

	return result;
}

path_finder::reachable_map path_finder::choppable(reachable_map const & reachable, int moves) const
{
	path_finder::reachable_map result;

	for (auto const & p : reachable)
	{
		for (int d = 0; d < 4; ++d)
		{
			auto t = p.first + directions[d];
			int cost = p.second.cost + 1;
			if (cost <= moves && trees_.has_tree(t[0], t[1]))
			{
				if (result.count(t) == 0 || result[t].cost > cost)
				{
					result[t].cost = cost;
					result[t].direction = d;
				}
			}
		}
	}

	return result;
}

path_finder::reachable_map path_finder::buildable(reachable_map const & reachable, int moves) const
{
	path_finder::reachable_map result;

	for (auto const & p : reachable)
	{
		for (int d = 0; d < 4; ++d)
		{
			auto t = p.first + directions[d];

			if (t[0] < 0 || t[0] >= map_.size[0] || t[1] < 0 || t[1] >= map_.size[1]) continue;

			int cost = p.second.cost + 1;
			if (cost <= moves && !trees_.has_tree(t[0], t[1]) && !units_.unit_at(t) && (!ramps_.has_ramp(t[0], t[1]) || ramps_.effective_geometry(t)->index() == 0))
			{
				int h0 = *ramps_.effective_height(p.first);
				int h1 = map_.height(t[0], t[1]);
				if (h0 == h1 || h0 == h1 + 1)
				{
					if (result.count(t) == 0 || result[t].cost > cost)
					{
						result[t].cost = cost;
						result[t].direction = d;
					}
				}
			}
		}
	}

	return result;
}
