#include <game/trees.hpp>

#include <psemek/random/generator.hpp>
#include <psemek/random/uniform.hpp>

#include <models/tree_trunk_obj.hpp>
#include <models/tree_leaves_obj.hpp>

static char const trees_vs[] =
R"(#version 330

uniform mat4 u_camera_transform;
uniform vec3 u_light_dir;
uniform vec3 u_color;

uniform vec3 u_position;
uniform float u_scale;
uniform vec2 u_rotation;

layout (location = 0) in vec3 in_position;
layout (location = 2) in vec3 in_normal;

out vec3 color;

vec3 rotate(vec3 p, vec2 r)
{
	return vec3(p.x * r.x - p.y * r.y, p.x * r.y + p.y * r.x, p.z);
}

void main()
{
	gl_Position = u_camera_transform * vec4(u_position + rotate(in_position, u_rotation) * u_scale, 1.0);
	color = (0.5 + 0.5 * dot(u_light_dir, rotate(in_normal, u_rotation))) * u_color;
}
)";

static char const trees_fs[] =
R"(#version 330

in vec3 color;

out vec4 out_color;

void main()
{
	out_color = vec4(color, 1.0);
}
)";

trees::trees(map const & m)
	: program_{trees_vs, trees_fs}
{
	trunk_mesh_ = load_mesh(models::tree_trunk_obj);
	leaves_mesh_ = load_mesh(models::tree_leaves_obj);

	trees_.resize(m.height.dims());

	random::generator rng;

	for (int y = 0; y < trees_.dim(1); ++y)
	{
		for (int x = 0; x < trees_.dim(0); ++x)
		{
			float s = m.tree_size(x, y);
			if (s == 0.f) continue;

			trees_(x, y).position[0] = x + random::uniform_distribution<float>(0.3f, 0.7f)(rng);
			trees_(x, y).position[1] = y + random::uniform_distribution<float>(0.3f, 0.7f)(rng);
			trees_(x, y).position[2] = *m.tile_center_height(x, y);
			trees_(x, y).size = s;
			trees_(x, y).rotation = random::uniform_distribution<float>(0.f, 2.f * geom::pi)(rng);
		}
	}
}

bool trees::has_tree(int x, int y) const
{
	if (x < 0 || x >= trees_.dim(0) || y < 0 || y >= trees_.dim(1)) return false;
	return trees_(x, y).size > 0.f;
}

void trees::remove_tree(int x, int y)
{
	if (trees_(x, y).size == 0 || trees_(x, y).dying) return;

	trees_(x, y).dying = 1.f;
	++dying_trees_;
}

void trees::update(float dt)
{
	for (int y = 0; y < trees_.dim(1); ++y)
	{
		for (int x = 0; x < trees_.dim(0); ++x)
		{
			auto & t = trees_(x, y);
			if (t.size == 0.f) continue;

			if (t.dying)
			{
				*t.dying -= dt;
				if (*t.dying <= 0.f)
				{
					t.size = 0.f;
					--dying_trees_;
				}
			}
		}
	}
}

bool trees::has_animation() const
{
	return dying_trees_ > 0;
}

void trees::render(render_options const & options)
{
	program_.bind();
	program_["u_camera_transform"] = options.camera_transform;
	program_["u_light_dir"] = options.light_dir;

	program_["u_color"] = geom::vector{0.25f, 0.f, 0.f};
	trunk_mesh_.bind();
	for (int y = 0; y < trees_.dim(1); ++y)
	{
		for (int x = 0; x < trees_.dim(0); ++x)
		{
			float s = trees_(x, y).size;
			if (s == 0.f) continue;

			float d = 0.f;
			if (trees_(x, y).dying)
				d -= (1.f - *trees_(x, y).dying) * 2.f;

			float r = trees_(x, y).rotation;

			program_["u_position"] = trees_(x, y).position + geom::vector{0.f, 0.f, d};
			program_["u_scale"] = s;
			program_["u_rotation"] = geom::vector{std::cos(r), std::sin(r)};
			gl::DrawArrays(gl::TRIANGLES, 0, trunk_mesh_.vertex_count());
		}
	}

	program_["u_color"] = geom::vector{0.f, 0.5f, 0.f};
	leaves_mesh_.bind();
	for (int y = 0; y < trees_.dim(1); ++y)
	{
		for (int x = 0; x < trees_.dim(0); ++x)
		{
			float s = trees_(x, y).size;
			if (s == 0.f) continue;

			float r = trees_(x, y).rotation;

			float d = 0.f;
			if (trees_(x, y).dying)
				d -= (1.f - *trees_(x, y).dying) * 2.f;

			program_["u_position"] = trees_(x, y).position + geom::vector{0.f, 0.f, d};
			program_["u_scale"] = s;
			program_["u_rotation"] = geom::vector{std::cos(r), std::sin(r)};
			gl::DrawArrays(gl::TRIANGLES, 0, leaves_mesh_.vertex_count());
		}
	}
}
