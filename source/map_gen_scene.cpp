#include <game/map_gen_scene.hpp>
#include <game/battle_scene.hpp>
#include <game/map.hpp>
#include <game/button.hpp>
#include <game/player.hpp>

#include <psemek/gfx/gl.hpp>
#include <psemek/gfx/painter.hpp>
#include <psemek/gfx/texture.hpp>

#include <psemek/geom/camera.hpp>
#include <psemek/geom/swizzle.hpp>

#include <psemek/random/device.hpp>
#include <psemek/random/generator.hpp>

#include <psemek/util/to_string.hpp>

#include <buttons/up_ppm.hpp>
#include <buttons/down_ppm.hpp>

struct units;

namespace
{

struct generation_options
{
	int width = 64;
	int height = 32;
	int trees_density = 6;
	int river_width = 3;
	int bridges = 5;

	struct player_options
	{
		int type = 0;
		int army = 5;
	};
	player_options players[2];
};

static int const trees_density_max = 12;

float tree_density_value(int value)
{
	return value * 0.05f;
}

static int const panel_margin = 32;
static int const bottom_button_height = 64;

static std::vector<geom::vector<int, 2>> const sizes =
{
	{16, 16},
	{32, 16},
	{32, 32},
	{64, 32},
	{64, 64},
	{128, 64},
	{128, 128},
	{256, 128},
};

struct map_generation_scene
	: app::scene_base
{
	map_generation_scene(std::function<void()> on_exit, std::function<void(std::unique_ptr<app::scene>)> on_change_scene);

	void on_resize(int width, int height) override;
	void on_mouse_move(int x, int y, int dx, int dy) override;
	void on_left_button_down() override;
	void on_left_button_up() override;

	void present() override;

private:
	std::function<void()> on_exit_;
	std::function<void(std::unique_ptr<app::scene>)> on_change_scene_;

	generation_options options_;
	random::generator rng_;

	map map_;

	std::optional<button> map_panel_;
	std::optional<button> player_panel_[2];

	button back_button_;
	button start_button_;

	button size_inc_button_;
	button size_dec_button_;
	button trees_inc_button_;
	button trees_dec_button_;
	button river_inc_button_;
	button river_dec_button_;
	button bridges_inc_button_;
	button bridges_dec_button_;

	button randomize_button_;

	std::optional<button> player_type_inc_button_[2];
	std::optional<button> player_type_dec_button_[2];
	std::optional<button> player_army_inc_button_[2];
	std::optional<button> player_army_dec_button_[2];

	std::vector<button *> buttons_;
	std::vector<button *> option_buttons_;

	int option_x_ = 0;
	std::vector<int> option_y_;

	std::optional<std::string> error_str_;

	gfx::texture_2d map_texture_;

	gfx::painter painter_;

	void randomize();
	void generate();
	void update_preview();
};

static auto up_texture()
{
	return load_transparent_texture(buttons::up_ppm, {255, 255, 255});
}

static auto down_texture()
{
	return load_transparent_texture(buttons::down_ppm, {255, 255, 255});
}

map_generation_scene::map_generation_scene(std::function<void()> on_exit, std::function<void(std::unique_ptr<app::scene>)> on_change_scene)
	: on_exit_(std::move(on_exit))
	, on_change_scene_(std::move(on_change_scene))
	, back_button_("Back", 200, bottom_button_height)
	, start_button_("Start", 200, bottom_button_height)
	, size_inc_button_(up_texture(), 1, "")
	, size_dec_button_(down_texture(), 1, "")
	, trees_inc_button_(up_texture(), 1, "")
	, trees_dec_button_(down_texture(), 1, "")
	, river_inc_button_(up_texture(), 1, "")
	, river_dec_button_(down_texture(), 1, "")
	, bridges_inc_button_(up_texture(), 1, "")
	, bridges_dec_button_(down_texture(), 1, "")
	, randomize_button_("Randomize", 300, bottom_button_height)
{
	back_button_.set_on_click([this]{
		on_exit_();
	});

	start_button_.set_on_click([this]{
		random::device device;
		auto rand = [&]{ return random::generator{device}; };

		player_controller_factory factory[2];
		for (int p = 0; p < 2; ++p)
		{
			factory[p] = [this, p, rand](units const & u, path_finder const & pf){
				if (options_.players[p].type == 0)
					return make_human_controller(p, u);
				else if (options_.players[p].type == 1)
					return make_offensive_ai_controller(p, u, pf, rand());
				else if (options_.players[p].type == 2)
					return make_defensive_ai_controller(p, u, pf, rand());
				else
					return make_dummy_ai_controller();
			};
		}

		auto battle_scene = make_battle_scene(std::move(map_), std::move(on_exit_), factory[0], factory[1]);
		on_change_scene_(std::move(battle_scene));
	});

	size_inc_button_.set_on_click([this]{
		geom::vector<int, 2> s{options_.width, options_.height};

		std::optional<int> j;
		for (int i = 0; i < sizes.size(); ++i)
		{
			if (s == sizes[i])
			{
				j = i;
				break;
			}
		}

		if (j)
		{
			if (*j + 1 < sizes.size())
			{
				s = sizes[*j + 1];
				options_.width = s[0];
				options_.height = s[1];
				generate();
			}
		}
	});

	size_dec_button_.set_on_click([this]{
		geom::vector<int, 2> s{options_.width, options_.height};

		std::optional<int> j;
		for (int i = 0; i < sizes.size(); ++i)
		{
			if (s == sizes[i])
			{
				j = i;
				break;
			}
		}

		if (j)
		{
			if (*j > 0)
			{
				s = sizes[*j - 1];
				options_.width = s[0];
				options_.height = s[1];
				generate();
			}
		}
	});

	trees_inc_button_.set_on_click([this]{
		if (options_.trees_density < trees_density_max)
		{
			++options_.trees_density;
			generate();
		}
	});

	trees_dec_button_.set_on_click([this]{
		if (options_.trees_density > 0)
		{
			--options_.trees_density;
			generate();
		}
	});

	river_inc_button_.set_on_click([this]{
		if (options_.river_width < 10)
		{
			++options_.river_width;
			generate();
		}
	});

	river_dec_button_.set_on_click([this]{
		if (options_.river_width > 0)
		{
			--options_.river_width;
			generate();
		}
	});

	bridges_inc_button_.set_on_click([this]{
		if (options_.bridges < 20)
		{
			++options_.bridges;
			generate();
		}
	});

	bridges_dec_button_.set_on_click([this]{
		if (options_.bridges > 0)
		{
			--options_.bridges;
			generate();
		}
	});

	randomize_button_.set_on_click([this]{
		randomize();
	});

	auto update_start_button = [this]{
		if (options_.players[0].type == 3 && options_.players[1].type == 3)
			start_button_.set_text("Really?");
		else
			start_button_.set_text("Start");
	};

	for (int p = 0; p < 2; ++p)
	{
		player_type_inc_button_[p] = button(up_texture(), 1, "");
		player_type_inc_button_[p]->set_on_click([this, p, update_start_button]{
			options_.players[p].type = (options_.players[p].type + 1) % 4;
			update_start_button();
		});

		player_type_dec_button_[p] = button(down_texture(), 1, "");
		player_type_dec_button_[p]->set_on_click([this, p, update_start_button]{
			options_.players[p].type = (options_.players[p].type + 3) % 4;
			update_start_button();
		});

		player_army_inc_button_[p] = button(up_texture(), 1, "");
		player_army_inc_button_[p]->set_on_click([this, p]{
			options_.players[p].army += 1;
			generate();
		});

		player_army_dec_button_[p] = button(down_texture(), 1, "");
		player_army_dec_button_[p]->set_on_click([this, p]{
			if (options_.players[p].army > 1)
			{
				options_.players[p].army -= 1;
				generate();
			}
		});
	}

	buttons_ =
	{
		&back_button_,
		&start_button_,
		&size_inc_button_,
		&size_dec_button_,
		&trees_inc_button_,
		&trees_dec_button_,
		&river_inc_button_,
		&river_dec_button_,
		&bridges_inc_button_,
		&bridges_dec_button_,
		&randomize_button_,
		&*(player_type_inc_button_[0]),
		&*(player_type_inc_button_[1]),
		&*(player_type_dec_button_[0]),
		&*(player_type_dec_button_[1]),
		&*(player_army_inc_button_[0]),
		&*(player_army_inc_button_[1]),
		&*(player_army_dec_button_[0]),
		&*(player_army_dec_button_[1]),
	};

	option_buttons_ =
	{
		&size_inc_button_,
		&size_dec_button_,
		&trees_inc_button_,
		&trees_dec_button_,
		&river_inc_button_,
		&river_dec_button_,
		&bridges_inc_button_,
		&bridges_dec_button_,
	};

	option_y_.assign(option_buttons_.size() / 2, 0);

	map_texture_.nearest_filter();

	options_.players[1].type = 1;

	randomize();
}

void map_generation_scene::on_resize(int width, int height)
{
	scene_base::on_resize(width, height);
	gl::Viewport(0, 0, width, height);

	map_panel_ = button("", width - 2 * panel_margin, height / 2 - (3 * panel_margin) / 2 + 100);
	map_panel_->set_position({width / 2, panel_margin + map_panel_->height() / 2});

	player_panel_[0] = button("", width / 2 - (3 * panel_margin) / 2, height - map_panel_->height() - 4 * panel_margin - bottom_button_height);
	player_panel_[0]->set_position({player_panel_[0]->width() / 2 + panel_margin, map_panel_->height() + 2 * panel_margin + player_panel_[0]->height() / 2});

	player_panel_[1] = button("", width / 2 - (3 * panel_margin) / 2, height - map_panel_->height() - 4 * panel_margin - bottom_button_height);
	player_panel_[1]->set_position({width - player_panel_[1]->width() / 2 - panel_margin, map_panel_->height() + 2 * panel_margin + player_panel_[1]->height() / 2});

	back_button_.set_position({panel_margin + back_button_.width() / 2, height - panel_margin - back_button_.height() / 2});
	start_button_.set_position({width - panel_margin - start_button_.width() / 2, height - panel_margin - start_button_.height() / 2});

	option_x_ = map_panel_->position()[0] - map_panel_->width() / 4 + 200;
	int option_y = map_panel_->position()[1] - map_panel_->height() / 2 + 2 * panel_margin;

	for (int i = 0; i < option_buttons_.size(); i += 2)
	{
		int w = option_buttons_[i + 0]->width();
		int h = option_buttons_[i + 0]->height();
		int s = 2;
		option_buttons_[i + 0]->set_position({option_x_ + w / 2, option_y - h / 2 - s});
		option_buttons_[i + 1]->set_position({option_x_ + w / 2, option_y + h / 2 + s});

		option_y_[i / 2] = option_y;

		option_y += 48;
	}

	randomize_button_.set_position({option_x_ - randomize_button_.width() / 2, option_y + 32});
}

void map_generation_scene::on_mouse_move(int x, int y, int dx, int dy)
{
	scene_base::on_mouse_move(x, y, dx, dy);

	for (auto & b : buttons_)
		b->on_mouse_move({x, y});
}

void map_generation_scene::on_left_button_down()
{
	scene_base::on_left_button_down();

	for (auto & b : buttons_)
		if (b->on_mouse_down()) return;
}

void map_generation_scene::on_left_button_up()
{
	scene_base::on_left_button_up();

	for (auto & b : buttons_)
		b->on_mouse_up();
}

void map_generation_scene::present()
{
	gl::ClearColor(0.8f, 0.8f, 1.f, 1.f);
	gl::Clear(gl::COLOR_BUFFER_BIT | gl::DEPTH_BUFFER_BIT);

	if (map_panel_)
		map_panel_->render(painter_);
	if (player_panel_[0])
		player_panel_[0]->render(painter_);
	if (player_panel_[1])
		player_panel_[1]->render(painter_);

	for (auto & b : buttons_)
		if (b->active())
			b->render(painter_);

	if (map_panel_)
	{
		geom::box<float, 2> map_box;
		map_box[0].min = map_panel_->position()[0] + panel_margin / 2;
		map_box[0].max = map_panel_->position()[0] + map_panel_->width() / 2 - panel_margin;
		map_box[1].min = map_panel_->position()[1] - map_panel_->height() / 2 + panel_margin;
		map_box[1].max = map_panel_->position()[1] + map_panel_->height() / 2 - panel_margin;

		float aspect_ratio = (1.f * map_texture_.width()) / map_texture_.height();

		if (map_box[1].length() < map_box[0].length() / aspect_ratio)
		{
			float s = map_box[1].length() * aspect_ratio;
			map_box[0] = {map_box[0].center() - s / 2.f, map_box[0].center() + s / 2.f};
		}
		else
		{
			float s = map_box[0].length() / aspect_ratio;
			map_box[1] = {map_box[1].center() - s / 2.f, map_box[1].center() + s / 2.f};
		}

		painter_.texture(map_texture_, map_box);
	}

	std::vector<std::string> option_key =
	{
		"Map size:",
		"Tree density:",
		"River:",
		"Bridges:",
	};

	std::vector<std::string> option_value =
	{
		util::to_string(options_.width, "x", options_.height),
		util::to_string(static_cast<int>(tree_density_value(options_.trees_density) * 100), "%"),
		util::to_string(options_.river_width),
		util::to_string(options_.bridges),
	};

	for (int i = 0; i < option_y_.size(); ++i)
	{
		int x = option_x_ - 200;
		int y = option_y_[i];

		gfx::painter::text_options opts;
		opts.scale = 2.f;
		opts.x = gfx::painter::x_align::right;
		opts.y = gfx::painter::y_align::center;

		opts.c = {0, 0, 0, 255};
		painter_.text({x + 1, y + 1}, option_key[i], opts);
		opts.c = {255, 255, 255, 255};
		painter_.text({x, y}, option_key[i], opts);

		x = option_x_ - 10;
		opts.c = {0, 0, 0, 255};
		painter_.text({x + 1, y + 1}, option_value[i], opts);
		opts.c = {255, 255, 255, 255};
		painter_.text({x, y}, option_value[i], opts);
	}

	if (map_panel_ && error_str_)
	{
		gfx::painter::text_options opts;
		opts.scale = 2.f;
		opts.x = gfx::painter::x_align::left;
		opts.y = gfx::painter::y_align::bottom;

		int x = map_panel_->position()[0] - map_panel_->width() / 2 + panel_margin;
		int y = map_panel_->position()[1] + map_panel_->height() / 2 - panel_margin;

		opts.c = {0, 0, 0, 255};
		painter_.text({x + 1, y + 1}, *error_str_, opts);
		opts.c = {255, 0, 0, 255};
		painter_.text({x, y}, *error_str_, opts);
	}

	for (int p = 0; p < 2; ++p)
	{
		if (!player_panel_[p]) continue;

		int x = player_panel_[p]->position()[0] - 100;
		int y = player_panel_[p]->position()[1] - player_panel_[p]->height() / 2 + panel_margin;

		{
			gfx::painter::text_options opts;
			opts.scale = 2.f;
			opts.x = gfx::painter::x_align::right;
			opts.y = gfx::painter::y_align::center;

			auto str = util::to_string("Player ", p + 1, ":");

			opts.c = {0, 0, 0, 255};
			painter_.text({x + 1, y + 1}, str, opts);
			opts.c = gfx::to_coloru8(player_colors[p]);
			painter_.text({x, y}, str, opts);

			str.clear();
			if (options_.players[p].type == 0)
				str = "Human";
			else if (options_.players[p].type == 1)
				str = "AI (offensive)";
			else if (options_.players[p].type == 2)
				str = "AI (defensive)";
			else if (options_.players[p].type == 3)
				str = "AI (dummy)";

			int xx = x + 260;

			opts.c = {0, 0, 0, 255};
			painter_.text({xx + 1, y + 1}, str, opts);
			opts.c = {255, 255, 255, 255};
			painter_.text({xx, y}, str, opts);

			int w = player_type_inc_button_[p]->width();
			int h = player_type_inc_button_[p]->height();

			player_type_inc_button_[p]->set_position({xx + w / 2, y - h / 2 - 2});
			player_type_inc_button_[p]->render(painter_);

			player_type_dec_button_[p]->set_position({xx + w / 2, y + h / 2 + 2});
			player_type_dec_button_[p]->render(painter_);
		}

		y += 48;

		{
			gfx::painter::text_options opts;
			opts.scale = 2.f;
			opts.x = gfx::painter::x_align::right;
			opts.y = gfx::painter::y_align::center;

			auto str = util::to_string("Army size:");

			opts.c = {0, 0, 0, 255};
			painter_.text({x + 1, y + 1}, str, opts);
			opts.c = gfx::to_coloru8(player_colors[p]);
			painter_.text({x, y}, str, opts);

			str = util::to_string(options_.players[p].army);

			int xx = x + 260;

			opts.c = {0, 0, 0, 255};
			painter_.text({xx + 1, y + 1}, str, opts);
			opts.c = {255, 255, 255, 255};
			painter_.text({xx, y}, str, opts);

			int w = player_army_inc_button_[p]->width();
			int h = player_army_inc_button_[p]->height();

			player_army_inc_button_[p]->set_position({xx + w / 2, y - h / 2 - 2});
			player_army_inc_button_[p]->render(painter_);

			player_army_dec_button_[p]->set_position({xx + w / 2, y + h / 2 + 2});
			player_army_dec_button_[p]->render(painter_);
		}
	}

	painter_.render(geom::window_camera{width(), height()}.transform());
}

void map_generation_scene::randomize()
{
	rng_ = random::generator{random::device{}};
	generate();
}

void map_generation_scene::generate()
{
	error_str_ = std::nullopt;

	try
	{
		random::generator rng = rng_;
		map_ = generate_map(options_.width, options_.height, tree_density_value(options_.trees_density), options_.river_width, options_.bridges, std::move(rng),
			options_.players[0].army, options_.players[1].army);
		start_button_.set_active(true);
	}
	catch (std::exception const & e)
	{
		error_str_ = std::string(e.what()) + ", try a different map";
		start_button_.set_active(false);
	}

	update_preview();
}

void map_generation_scene::update_preview()
{
	static gfx::color_rgb const ground_color{32, 128, 32};

	static geom::vector<float, 3> const normals[4] =
	{
		geom::normalized(geom::vector{-1.f, 0.f, 1.f}),
		geom::normalized(geom::vector{ 1.f, 0.f, 1.f}),
		geom::normalized(geom::vector{0.f, -1.f, 1.f}),
		geom::normalized(geom::vector{0.f,  1.f, 1.f}),
	};

	static auto const light = light_dir();

	if (error_str_)
	{
		gfx::pixmap_rgb pm({options_.width, options_.height}, gfx::color_rgb{0, 0, 0});
		map_texture_.load(pm);
		return;
	}

	gfx::pixmap_rgb pm({map_.size[0], map_.size[1]}, ground_color);

	for (int y = 0; y < map_.size[1]; ++y)
	{
		for (int x = 0; x < map_.size[0]; ++x)
		{
			auto g = map_.geometry(x, y);
			if (map_.height(x, y) == 0 && std::get_if<flat>(&g))
				pm(x, y) = {64, 64, 192};

			if (map_.tree_size(x, y) > 0.f)
				pm(x, y) = {0, 64, 0};

			if (map_.bridges(x, y) > 0)
				pm(x, y) = {100, 50, 0};

			geom::vector<float, 3> n{0.f, 0.f, 1.f};

			if (auto s = std::get_if<slope>(&g))
			{
				n = normals[s->direction];
			}

			float l = 0.5f + 0.5f * geom::dot(n, light);
			pm(x, y) = gfx::to_coloru8(1.f * l * gfx::to_colorf(pm(x, y)));
		}
	}

	for (int p = 0; p < 2; ++p)
	{
		for (auto const & u : map_.units[p])
		{
			pm(u.second[0], u.second[1]) = gfx::to_coloru8(geom::swizzle<0, 1, 2>(player_colors[p]));
		}
	}

	util::mirror(pm, 1);

	map_texture_.load(pm);
}

}

std::unique_ptr<app::scene> make_map_generation_scene(std::function<void()> on_exit, std::function<void(std::unique_ptr<app::scene>)> on_change_scene)
{
	return std::make_unique<map_generation_scene>(std::move(on_exit), std::move(on_change_scene));
}
