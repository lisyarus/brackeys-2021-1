#include <game/selection.hpp>

static char const selection_vs[] =
R"(#version 330

uniform mat4 u_camera_transform;

layout (location = 0) in vec3 in_position;
layout (location = 1) in vec4 in_color;

out vec4 color;

void main()
{
	gl_Position = u_camera_transform * vec4(in_position, 1.0);
	color = in_color;
}
)";

static char const selection_fs[] =
R"(#version 330

in vec4 color;

out vec4 out_color;

void main()
{
	out_color = color;
}
)";

selection::selection()
	: program_{selection_vs, selection_fs}
{
	vao_.bind();
	vbo_.bind();
	gl::EnableVertexAttribArray(0);
	gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE, 16, 0);
	gl::EnableVertexAttribArray(1);
	gl::VertexAttribPointer(1, 4, gl::UNSIGNED_BYTE, gl::TRUE, 16, reinterpret_cast<void const *>(12));
}

void selection::render(render_options const & options, std::vector<tile> const & tiles)
{
	struct vertex
	{
		geom::point<float, 3> position;
		gfx::color_rgba color;
	};

	static_assert(sizeof(vertex) == 16);

	struct face
	{
		geom::point<float, 3> points[4];
		gfx::color_4f colors[4];
		float dist{};
	};

	std::vector<face> faces;

	for (tile const & t : tiles)
	{
		auto c = geom::lerpn(t.points[0], 0.25f, t.points[1], 0.25f, t.points[2], 0.25f, t.points[3], 0.25f);

		auto points = t.points;

		for (int i = 0; i < 4; ++i)
			points[i] = c + (points[i] - c) * 0.9375f;

		if (t.height > 0.f)
		{
			geom::vector<float, 3> const z{0.f, 0.f, t.height};

			face f;
			f.colors[0] = t.color;
			f.colors[1] = t.color;
			f.colors[2] = t.color;
			f.colors[3] = t.color;
			f.colors[2][3] = 0.f;
			f.colors[3][3] = 0.f;

			auto push_face = [&](int i, int j)
			{
				f.points[0] = points[i];
				f.points[1] = points[j];
				f.points[2] = points[i] + z;
				f.points[3] = points[j] + z;

				faces.push_back(f);
			};

			push_face(0, 1);
			push_face(1, 3);
			push_face(3, 2);
			push_face(2, 0);
		}
		else
		{
			geom::vector<float, 3> const z{0.f, 0.f, 0.03125f};

			face f;
			f.colors[0] = t.color;
			f.colors[1] = t.color;
			f.colors[2] = t.color;
			f.colors[3] = t.color;
			f.points[0] = points[0] + z;
			f.points[1] = points[1] + z;
			f.points[2] = points[2] + z;
			f.points[3] = points[3] + z;
			faces.push_back(f);
		}
	}

	for (face & f : faces)
	{
		auto c = geom::lerpn(f.points[0], 0.25f, f.points[1], 0.25f, f.points[2], 0.25f, f.points[3], 0.25f);
		f.dist = geom::dot(c - options.camera_pos, options.camera_dir);
	}

	std::sort(faces.begin(), faces.end(), [&options](face const & f0, face const & f1){
		return f0.dist > f1.dist;
	});

	program_.bind();
	program_["u_camera_transform"] = options.camera_transform;

	vao_.bind();

	vertex vertices[4];
	for (face const & f : faces)
	{
		for (int i = 0; i < 4; ++i)
			vertices[i] = {f.points[i], gfx::to_coloru8(f.colors[i])};

		vbo_.load(vertices, gl::STREAM_DRAW);
		gl::DrawArrays(gl::TRIANGLE_STRIP, 0, 4);
	}
}
