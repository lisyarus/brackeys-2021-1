#include <game/settings.hpp>

#include <psemek/log/log.hpp>

#include <fstream>

using namespace psemek;

void settings::load(std::filesystem::path path)
{
	log::info() << "Loading settings from " << path;
	std::ifstream fs(path);
	auto saved = *this;
	fs >> auto_camera;
	fs >> inverted_y;
	fs >> inverted_wheel;
	fs >> mouse_sensitivity;
	fs >> sound_volume;
	if (!fs)
	{
		(*this) = saved;
		log::info() << "Failed to load settings";
	}
}

void settings::save(std::filesystem::path path)
{
	log::info() << "Saving settings to " << path;

	std::ofstream fs(path);
	if (!fs)
	{
		log::info() << "Failed to save settings";
		return;
	}

	fs << auto_camera << ' ' << inverted_y << ' ' << inverted_wheel << ' ' << mouse_sensitivity << ' ' << sound_volume << ' ';
}

settings & global_settings()
{
	static settings s;
	return s;
}
