#include <game/terrain.hpp>

#include <psemek/random/generator.hpp>
#include <psemek/random/uniform.hpp>

static const char terrain_vs[] =
R"(#version 330

uniform mat4 u_camera_transform;
uniform float u_time;

layout (location = 0) in vec3 in_position;
layout (location = 1) in vec3 in_texcoord;
layout (location = 2) in float in_extra;

out vec3 g_position;
out vec3 g_texcoord;
out float g_specular;

void main()
{
	g_position = in_position;
	if (in_texcoord.z == 2.0)
	{
		// water
		g_specular = 64.0;
		if (in_extra == 1.0)
		{
			g_position.z += (sin(g_position.x + u_time) + sin(g_position.y + u_time * 2.15) + 0.25 * sin(g_position.x * 2.5 + g_position.y * 1.78)) * 0.1;
		}
	}
	else
	{
		g_specular = 0.0;
	}
	gl_Position = u_camera_transform * vec4(g_position, 1.0);
	g_texcoord = in_texcoord;
}
)";

static const char terrain_gs[] =
R"(#version 330

layout (triangles) in;
layout (triangle_strip, max_vertices = 3) out;

in vec3 g_position[];
in vec3 g_texcoord[];
in float g_specular[];

out vec3 position;
out vec3 normal;
out vec3 texcoord;
out float specular;

void main()
{
	normal = normalize(cross(g_position[1] - g_position[0], g_position[2] - g_position[0]));

	for (int i = 0; i < 3; ++i)
	{
		gl_Position = gl_in[i].gl_Position;
		position = g_position[i];
		texcoord = g_texcoord[i];
		specular = g_specular[i];
		EmitVertex();
	}
	EndPrimitive();
}

)";

static const char terrain_fs[] =
R"(#version 330

uniform vec3 u_light_dir;
uniform vec3 u_camera_pos;
uniform sampler2DArray u_texture;

in vec3 position;
in vec3 normal;
in vec3 texcoord;
in float specular;

layout (location = 0) out vec4 out_color;

void main()
{
	vec3 n = normalize(normal);
	vec3 color = texture(u_texture, texcoord).rgb;
	float l = 0.5 + 0.5 * dot(n, u_light_dir);
	float s = 0.0;
	if (specular != 0.0)
	{
		vec3 r = normalize(u_camera_pos - position);
		r = 2 * n * dot(r, n) - r;
		s = pow(max(0.0, dot(r, u_light_dir)), specular);
	}
	out_color = vec4(color * l + vec3(s), 1.0);
}
)";

terrain::terrain(map const & m)
	: program_{terrain_vs, terrain_gs, terrain_fs}
{
	generate_texture();
	generate_buffers(m);
}

void terrain::generate_texture()
{
	random::generator rng;

	auto rand = [&rng](int min, int max){ return random::uniform_distribution<int>{min, max}(rng); };

	util::array<gfx::color_rgb, 3> pm({32, 32, 3});

	// 0 - grid grass
	for (int y = 0; y < 32; ++y)
	{
		for (int x = 0; x < 32; ++x)
		{
			if (x == 0 || x + 1 == 32 || y == 0 || y + 1 == 32)
				pm(x, y, 0) = {64, 64, 64};
			else
				pm(x, y, 0) = {rand(64, 128), rand(128, 196), rand(0, 64)};
		}
	}

	// 1 - dirt
	for (int y = 0; y < 32; ++y)
	{
		for (int x = 0; x < 32; ++x)
		{
			pm(x, y, 1) = {rand(32, 64), rand(16, 32), rand(0, 0)};
		}
	}

	// 2 - water
	for (int y = 0; y < 32; ++y)
	{
		for (int x = 0; x < 32; ++x)
		{
			pm(x, y, 2) = {rand(16, 32), rand(64, 128), rand(128, 196)};
		}
	}

	texture_.load(pm);
	gl::TexParameteri(texture_.target, gl::TEXTURE_MIN_FILTER, gl::LINEAR_MIPMAP_LINEAR);
	gl::TexParameteri(texture_.target, gl::TEXTURE_MAG_FILTER, gl::NEAREST);
	texture_.anisotropy();
	texture_.clamp();
	texture_.generate_mipmap();
}

void terrain::generate_buffers(map const & m)
{
	struct vertex
	{
		geom::point<float, 3> position;
		geom::vector<std::uint8_t, 3> texcoord;
		std::uint8_t extra = 0;
	};

	std::vector<vertex> vertices;
	std::vector<std::uint32_t> indices;

	// top faces
	for (int y = 0; y < m.size[1]; ++y)
	{
		for (int x = 0; x < m.size[0]; ++x)
		{
			int h = m.height(x, y);

			std::uint32_t const base = vertices.size();

			auto g = m.geometry(x, y);

			if (std::get_if<flat>(&g))
			{
				auto midwater = [&](int tx, int ty){
					int xx = x + tx;
					int yy = y + ty;
					return (xx > 0 && xx + 1 < m.size[0] && yy > 0 && yy + 1 < m.size[1]
						&& m.height(xx - 1, yy - 1) == 0 && m.geometry(xx - 1, yy - 1) == tile{flat{}}
						&& m.height(xx    , yy - 1) == 0 && m.geometry(xx    , yy - 1) == tile{flat{}}
						&& m.height(xx - 1, yy    ) == 0 && m.geometry(xx - 1, yy    ) == tile{flat{}}
						&& m.height(xx    , yy    ) == 0 && m.geometry(xx    , yy    ) == tile{flat{}}
						) ? 1 : 0;
				};

				int t = (h > 0) ? 0 : 2;
				vertices.push_back({{x, y, h}, {0, 0, t}, midwater(0, 0)});
				vertices.push_back({{x + 1.f, y, h}, {1, 0, t}, midwater(1, 0)});
				vertices.push_back({{x, y + 1.f, h}, {0, 1, t}, midwater(0, 1)});
				vertices.push_back({{x + 1.f, y + 1.f, h}, {1, 1, t}, midwater(1, 1)});
				indices.insert(indices.end(), {base, base + 1, base + 2, base + 2, base + 1, base + 3});
			}
			else if (auto s = std::get_if<slope>(&g))
			{
				int dx = directions[s->direction][0];
				int dy = directions[s->direction][1];

				vertices.push_back({{x, y, h + (dx == -1 ? 1 : 0) + (dy == -1 ? 1 : 0)}, {0, 0, 0}});
				vertices.push_back({{x + 1.f, y, h + (dx == 1 ? 1 : 0) + (dy == -1 ? 1 : 0)}, {1, 0, 0}});
				vertices.push_back({{x, y + 1.f, h + (dx == -1 ? 1 : 0) + (dy == 1 ? 1 : 0)}, {0, 1, 0}});
				vertices.push_back({{x + 1.f, y + 1.f, h + (dx == 1 ? 1 : 0) + (dy == 1 ? 1 : 0)}, {1, 1, 0}});
				indices.insert(indices.end(), {base, base + 1, base + 2, base + 2, base + 1, base + 3});

				if (dy == 0)
				{
					int ix = (dx == 1 ? 1 : 0);

					auto n = geom::vector{0.f, -1.f, 0.f};
					vertices.push_back({{x, y, h}, {0, 0, 1}});
					vertices.push_back({{x + 1.f, y, h}, {1, 0, 1}});
					vertices.push_back({{x + ix, y, h + 1.f}, {ix, 1, 1}});
					indices.insert(indices.end(), {base + 4, base + 5, base + 6});

					n = geom::vector{0.f, 1.f, 0.f};
					vertices.push_back({{x + 1.f, y + 1.f, h}, {1, 0, 1}});
					vertices.push_back({{x, y + 1.f, h}, {0, 0, 1}});
					vertices.push_back({{x + ix, y + 1.f, h + 1.f}, {ix, 1, 1}});
					indices.insert(indices.end(), {base + 7, base + 8, base + 9});
				}

				if (dx == 0)
				{
					int iy = (dy == 1 ? 1 : 0);

					auto n = geom::vector{-1.f, 0.f, 0.f};
					vertices.push_back({{x, y + 1.f, h}, {1, 0, 1}});
					vertices.push_back({{x, y, h}, {0, 0, 1}});
					vertices.push_back({{x, y + iy, h + 1.f}, {iy, 1, 1}});
					indices.insert(indices.end(), {base + 4, base + 5, base + 6});

					n = geom::vector{1.f, 0.f, 0.f};
					vertices.push_back({{x + 1.f, y, h}, {0, 0, 1}});
					vertices.push_back({{x + 1.f, y + 1.f, h}, {1, 0, 1}});
					vertices.push_back({{x + 1.f, y + iy, h + 1.f}, {iy, 1, 1}});
					indices.insert(indices.end(), {base + 7, base + 8, base + 9});
				}
			}
		}
	}

	int const skirt_height = -5;

	// vertical faces orthogonal to X
	for (int y = 0; y < m.size[1]; ++y)
	{
		for (int x = 0; x <= m.size[0]; ++x)
		{
			bool const slope0 = (x == 0) ? false : m.geometry(x - 1, y) == tile{slope{0}};
			bool const slope1 = (x == m.size[0]) ? false : m.geometry(x, y) == tile{slope{2}};

			int const h0 = (x == 0) ? skirt_height : m.height(x - 1, y) + (slope0 ? 1 : 0);
			int const h1 = (x == m.size[0]) ? skirt_height : m.height(x, y) + (slope1 ? 1 : 0);

			int const dir = (h0 < h1) ? 1 : -1;

			for (int h = h0; h != h1; h += dir)
			{
				std::uint32_t const base = vertices.size();
				vertices.push_back({{x, y, h}, {0, 0, 1}});
				vertices.push_back({{x, y, h + dir}, {1, 0, 1}});
				vertices.push_back({{x, y + 1.f, h}, {0, 1, 1}});
				vertices.push_back({{x, y + 1.f, h + dir}, {1, 1, 1}});
				indices.insert(indices.end(), {base, base + 1, base + 2, base + 2, base + 1, base + 3});
			}
		}
	}

	// vertical faces orthogonal to Y
	for (int y = 0; y <= m.size[1]; ++y)
	{
		for (int x = 0; x < m.size[0]; ++x)
		{
			bool const slope0 = (y == 0) ? false : m.geometry(x, y - 1) == tile{slope{1}};
			bool const slope1 = (y == m.size[1]) ? false : m.geometry(x, y) == tile{slope{3}};

			int const h0 = (y == 0) ? skirt_height : m.height(x, y - 1) + (slope0 ? 1 : 0);
			int const h1 = (y == m.size[1]) ? skirt_height : m.height(x, y) + (slope1 ? 1 : 0);

			int const dir = (h0 < h1) ? 1 : -1;

			for (int h = h0; h != h1; h += dir)
			{
				std::uint32_t const base = vertices.size();
				vertices.push_back({{x + 1.f, y, h}, {0, 0, 1}});
				vertices.push_back({{x + 1.f, y, h + dir}, {1, 0, 1}});
				vertices.push_back({{x, y, h}, {0, 1, 1}});
				vertices.push_back({{x, y, h + dir}, {1, 1, 1}});
				indices.insert(indices.end(), {base, base + 1, base + 2, base + 2, base + 1, base + 3});
			}
		}
	}

	vao_.bind();
	vbo_.load(vertices, gl::STATIC_DRAW);
	gl::EnableVertexAttribArray(0);
	gl::VertexAttribPointer(0, 3, gl::FLOAT, gl::FALSE, sizeof(vertex), 0);
	gl::EnableVertexAttribArray(1);
	gl::VertexAttribPointer(1, 3, gl::UNSIGNED_BYTE, gl::FALSE, sizeof(vertex), reinterpret_cast<const void *>(12));
	gl::EnableVertexAttribArray(2);
	gl::VertexAttribPointer(2, 1, gl::UNSIGNED_BYTE, gl::FALSE, sizeof(vertex), reinterpret_cast<const void *>(15));

	ebo_.load(indices, gl::STATIC_DRAW);
	gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, ebo_.id());

	count_ = indices.size();
}

void terrain::render(render_options const & options)
{
	program_.bind();
	program_["u_time"] = options.time;
	program_["u_camera_transform"] = options.camera_transform;
	program_["u_camera_pos"] = options.camera_pos;
	program_["u_light_dir"] = options.light_dir;
	program_["u_texture"] = 0;

	gl::ActiveTexture(gl::TEXTURE0);
	texture_.bind();

	vao_.bind();
	gl::DrawElements(gl::TRIANGLES, count_, gl::UNSIGNED_INT, 0);
}
