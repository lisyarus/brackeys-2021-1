#include <game/map.hpp>

#include <psemek/pcg/perlin.hpp>

#include <psemek/random/uniform.hpp>
#include <psemek/random/uniform_sphere.hpp>

#include <set>

std::array<geom::point<float, 3>, 4> surface(tile t, int x, int y, int h)
{
	std::array<geom::point<float, 3>, 4> result;
	result[0] = {  x,   y, h};
	result[1] = {1+x,   y, h};
	result[2] = {  x, 1+y, h};
	result[3] = {1+x, 1+y, h};

	if (auto s = std::get_if<slope>(&t))
	{
		switch (s->direction)
		{
		case 0:
			result[1][2] += 1.f;
			result[3][2] += 1.f;
			break;
		case 1:
			result[2][2] += 1.f;
			result[3][2] += 1.f;
			break;
		case 2:
			result[0][2] += 1.f;
			result[2][2] += 1.f;
			break;
		case 3:
			result[0][2] += 1.f;
			result[1][2] += 1.f;
			break;
		default:
			assert(false);
		}
	}

	return result;
}

std::optional<std::array<geom::point<float, 3>, 4>> map::tile_surface(int x, int y) const
{
	if (x < 0 || x >= size[0] || y < 0 || y >= size[1]) return std::nullopt;

	auto const g = geometry(x, y);
	int const h = height(x, y);

	return surface(g, x, y, h);
}

std::optional<float> map::tile_center_height(int x, int y) const
{
	if (x < 0 || x >= size[0] || y < 0 || y >= size[1]) return std::nullopt;

	auto const g = geometry(x, y);
	int const h = height(x, y);

	if (g == tile{flat{}})
		return 1.f * h;

	return h + 0.5f;
}

map generate_map(int width, int height, float trees_density, int river_width, int bridge_count, random::generator && rng, int army0, int army1)
{
	map result;
	result.size = {width, height};
	result.height.resize({width, height}, 0);
	result.geometry.resize({width, height}, flat{});
	result.tree_size.resize({width, height}, 0.f);
	result.bridges.resize({width, height}, 0);

	result.wood_amount[0] = 50;
	result.wood_amount[1] = 50;

	// Heightmap
	{
		util::array<geom::vector<float, 2>, 2> grad_map({std::max(2, result.size[0] / 16 + 1), std::max(2, result.size[1] / 16 + 1)});
		random::uniform_sphere_vector_distribution<float, 2> d;
		for (auto & v : grad_map)
			v = d(rng);

		pcg::perlin<float, 2> perlin(std::move(grad_map));

		for (int y = 0; y < result.size[1]; ++y)
		{
			for (int x = 0; x < result.size[0]; ++x)
			{
				float v = perlin((x + 0.5f) / result.size[0], (y + 0.5f) / result.size[1]);
				v = 5.f * v - 1.f;

				if (river_width > 0)
				{
					float t = ((x + 0.5f) - result.size[0] / 2.f) / river_width;
					v -= 3.f * std::exp(- geom::sqr(t));
				}

				v = std::max(0.f, v);
				result.height(x, y) = std::round(v);
			}
		}
	}

	// tile geometry map
	{
		util::array<std::uint8_t, 2> slopes(result.height.dims(), 0);

		for (int iteration = 0; iteration < 2; ++iteration)
		{
			util::array<std::uint8_t, 2> new_slopes(slopes.dims(), 0);
			for (int y = 0; y < result.size[1]; ++y)
			{
				for (int x = 0; x < result.size[0]; ++x)
				{
					int h = result.height(x, y);
					if (x > 0 && x + 1 < result.size[0] && slopes(x - 1, y) == 0 && slopes(x + 1, y) == 0)
					{
						if (result.height(x - 1, y) == h && result.height(x + 1, y) == (h + 1))
							new_slopes(x, y) |= 1;
						if (result.height(x - 1, y) == h + 1 && result.height(x + 1, y) == h)
							new_slopes(x, y) |= 2;
					}
					if (y > 0 && y + 1 < result.size[1] && slopes(x, y - 1) == 0 && slopes(x, y + 1) == 0)
					{
						if (result.height(x, y - 1) == h && result.height(x, y + 1) == (h + 1))
							new_slopes(x, y) |= 4;
						if (result.height(x, y - 1) == h + 1 && result.height(x, y + 1) == h)
							new_slopes(x, y) |= 8;
					}
				}
			}
			slopes = std::move(new_slopes);
		}

		for (int y = 0; y < result.size[1]; ++y)
		{
			for (int x = 0; x < result.size[0]; ++x)
			{
				if (slopes(x, y) == 0) continue;

				tile set[4];
				tile * set_end = set;

				if (slopes(x, y) & 1)
					*set_end++ = slope{0};
				if (slopes(x, y) & 2)
					*set_end++ = slope{2};
				if (slopes(x, y) & 4)
					*set_end++ = slope{1};
				if (slopes(x, y) & 8)
					*set_end++ = slope{3};

				result.geometry(x, y) = set[random::uniform_distribution<int>(0, (set_end - set) - 1)(rng)];
			}
		}
	}

	// trees map
	{
		util::array<geom::vector<float, 2>, 2> grad_map({std::max(2, result.size[0] / 16 + 1), std::max(2, result.size[1] / 16 + 1)});
		{
			random::uniform_sphere_vector_distribution<float, 2> d;
			for (auto & v : grad_map)
				v = d(rng);
		}

		pcg::perlin<float, 2> perlin(std::move(grad_map));

		for (int y = 0; y < result.size[1]; ++y)
		{
			for (int x = 0; x < result.size[0]; ++x)
			{
				if (result.height(x, y) == 0) continue;

				if (perlin((x + 0.5f) / width, (y + 0.5f) / height) > trees_density) continue;

				result.tree_size(x, y) = random::uniform_distribution<float>(0.6f, 0.8f)(rng);
			}
		}
	}

	// bridges
	if (bridge_count > 0)
	{
		struct bridge_data
		{
			geom::point<int, 2> start;
			geom::vector<int, 2> dir;
			int length;
		};

		std::vector<bridge_data> possible_bridges;

		for (int y = 0; y < result.size[1]; ++y)
		{
			for (int x = 0; x < result.size[0]; ++x)
			{
				if (result.height(x, y) != 0) continue;

				auto const g = result.geometry(x, y);

				if (x > 0 && result.height(x-1, y) == 1 && (g == tile{flat{}} || g == tile{slope{2}}))
				{
					bool valid = false;
					int l = 1;
					for (; x + l < result.size[0]; ++l)
					{
						int h = result.height(x + l, y);
						auto g = result.geometry(x + l, y);

						if (h == 0 && g == tile{flat{}}) continue;

						if (h == 0 && g == tile{slope{0}})
						{
							++l;
							valid = true;
							break;
						}

						if (h == 0)
							break;

						if (h == 1)
						{
							valid = true;
							break;
						}

						break;
					}

					if (valid)
						possible_bridges.push_back({{x, y}, {1, 0}, l});
				}

				if (y > 0 && result.height(x, y-1) == 1 && (g == tile{flat{}} || g == tile{slope{3}}))
				{
					bool valid = false;
					int l = 1;
					for (; y + l < result.size[1]; ++l)
					{
						int h = result.height(x, y + l);
						auto g = result.geometry(x, y + l);

						if (h == 0 && g == tile{flat{}}) continue;

						if (h == 0 && g == tile{slope{1}})
						{
							++l;
							valid = true;
							break;
						}

						if (h == 0)
							break;

						if (h == 1)
						{
							valid = true;
							break;
						}

						break;
					}

					if (valid)
						possible_bridges.push_back({{x, y}, {0, 1}, l});
				}
			}
		}

		int bridges_constructed = 0;

		while (!possible_bridges.empty() && (bridges_constructed < bridge_count))
		{
			int i = random::uniform_distribution<int>(0, possible_bridges.size() - 1)(rng);

			if (i + 1 != possible_bridges.size())
				std::swap(possible_bridges[i], possible_bridges.back());

			bridge_data const bridge = possible_bridges.back();
			possible_bridges.pop_back();

			bool can_construct = true;

			for (int l = 0; l < bridge.length; ++l)
			{
				auto p = bridge.start + l * bridge.dir;
				if (result.bridges(p[0], p[1]) == 1)
				{
					can_construct = false;
					break;
				}
			}

			if (!can_construct) continue;

			for (int l = 0; l < bridge.length; ++l)
			{
				auto p = bridge.start + l * bridge.dir;
				result.bridges(p[0], p[1]) = 1;
			}
			++bridges_constructed;
		}
	}

	std::vector<geom::vector<int, 2>> formation[2];

	for (int player = 0; player < 2; ++player)
	{
		int count = (player == 0) ? army0 : army1;

		int const rows = std::ceil(std::sqrt(count + 1)) - 1;

		int const rstart = - rows / 2;
		int const rend = rstart + rows;

		for (int r = rstart; r < rend; ++r)
		{
			int const rcount = std::min(2 * (rend - r) + 1, count);
			count -= rcount;

			int const start = - rcount / 2;
			int const end = start + rcount;

			for (int c = start; c < end; ++c)
				formation[player].push_back(geom::vector{(player == 1) ? (rend - r - 1) : r, c});
		}
	}

	std::set<geom::point<int, 2>> placed_units;

	// units
	for (int player = 0; player < 2; ++player)
	{
		for (int type = 0; type < 3; ++type)
		{
			geom::interval<int> x_range;
			if (player == 0)
				x_range = {0, width / 8 - 1};
			else
				x_range = {7 * width / 8, width - 1};

			geom::interval<int> y_range{2, height - 3};

			geom::point<int, 2> c;
			for (int iteration = 0;; ++iteration)
			{
				if (iteration >= 1000)
					throw std::runtime_error("No space to place troops");

				c[0] = random::uniform_distribution<int>(x_range)(rng);
				c[1] = random::uniform_distribution<int>(y_range)(rng);

				if (result.height(c[0], c[1]) == 0)
					continue;

				bool found = true;
				for (auto d : formation[player])
				{
					auto p = c + d;
					if (p[0] < 0 || p[0] >= result.size[0] || p[1] < 0 || p[1] >= result.size[1])
					{
						found = false;
						break;
					}

					if (result.height(p[0], p[1]) == 0)
					{
						found = false;
						break;
					}

					if (result.tree_size(p[0], p[1]) > 0.f)
					{
						found = false;
						break;
					}

					if (placed_units.count(geom::point{p[0], p[1]}) > 0)
					{
						found = false;
						break;
					}
				}

				if (found)
					break;
			}

			for (auto d : formation[player])
			{
				auto p = c + d;
				placed_units.insert(p);
				result.units[player].push_back({static_cast<unit_type>(type), p});
			}
		}
	}

	return result;
}
