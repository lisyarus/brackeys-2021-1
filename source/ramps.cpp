#include <game/ramps.hpp>

#include <models/ramp_obj.hpp>
#include <models/ramp_slope_obj.hpp>

#include <psemek/random/generator.hpp>
#include <psemek/random/uniform.hpp>

static char const ramps_vs[] =
R"(#version 330

uniform mat4 u_camera_transform;
uniform vec3 u_light_dir;
uniform vec4 u_color;

uniform vec3 u_position;
uniform vec2 u_rotation;

layout (location = 0) in vec3 in_position;
layout (location = 2) in vec3 in_normal;

out vec4 color;

vec3 rotate(vec3 p, vec2 r)
{
	return vec3(p.x * r.x - p.y * r.y, p.x * r.y + p.y * r.x, p.z);
}

void main()
{
	gl_Position = u_camera_transform * vec4(u_position + rotate(in_position, u_rotation), 1.0);
	color = vec4((0.5 + 0.5 * dot(u_light_dir, rotate(in_normal, u_rotation))) * u_color.rgb, u_color.a);
}
)";

static char const ramps_fs[] =
R"(#version 330

in vec4 color;

out vec4 out_color;

void main()
{
	out_color = color;
}
)";

ramps::ramps(map const & m)
	: map_{m}
	, program_{ramps_vs, ramps_fs}
{
	flat_mesh_ = load_mesh(models::ramp_obj);
	slope_mesh_ = load_mesh(models::ramp_slope_obj);

	ramps_.resize(m.height.dims());

	for (int y = 0; y < ramps_.dim(1); ++y)
	{
		for (int x = 0; x < ramps_.dim(0); ++x)
		{
			if (m.bridges(x, y) != 1) continue;

			add_flat_ramp(x, y);
			ramps_(x, y).back().appearing = 1.f;
		}
	}
}

std::optional<tile> ramps::ramp_at(int x, int y) const
{
	if (x < 0 || x >= ramps_.dim(0) || y < 0 || y >= ramps_.dim(1)) return std::nullopt;
	if (ramps_(x, y).empty()) return std::nullopt;
	return ramps_(x, y).back().type;
}

bool ramps::has_ramp(int x, int y) const
{
	return ramp_at(x, y) != std::nullopt;
}

void ramps::add_ramp(geom::point<int, 2> const & t, tile type)
{
	if (std::get_if<flat>(&type))
	{
		add_flat_ramp(t[0], t[1]);
	}
	else if (auto s = std::get_if<slope>(&type))
	{
		add_slope_ramp(t[0], t[1], s->direction);
	}
}

void ramps::add_flat_ramp(int x, int y)
{
	int size = ramps_(x, y).size();
	auto & r = ramps_(x, y).emplace_back();
	r.type = flat{};
	r.position[0] = x + random::uniform_distribution<float>(0.4f, 0.6f)(rng_);
	r.position[1] = y + random::uniform_distribution<float>(0.4f, 0.6f)(rng_);
	r.position[2] = map_.height(x, y) + size + random::uniform_distribution<float>(-0.1f, 0.f)(rng_);
	r.rotation = random::uniform_distribution<int>(0, 3)(rng_) * (0.5f * geom::pi) + random::uniform_distribution<float>(-0.1f, 0.1f)(rng_);
	r.appearing = 0.f;
}

void ramps::add_slope_ramp(int x, int y, int direction)
{
	int size = ramps_(x, y).size();
	auto & r = ramps_(x, y).emplace_back();
	r.type = slope{direction};
	r.position[0] = x + random::uniform_distribution<float>(0.4f, 0.6f)(rng_);
	r.position[1] = y + random::uniform_distribution<float>(0.4f, 0.6f)(rng_);
	r.position[2] = map_.height(x, y) + size + random::uniform_distribution<float>(-0.1f, 0.f)(rng_);
	r.rotation = direction * (0.5f * geom::pi) + random::uniform_distribution<float>(-0.1f, 0.1f)(rng_);
	r.appearing = 0.f;
}

std::optional<std::array<geom::point<float, 3>, 4>> ramps::ramp_surface(int x, int y) const
{
	if (!has_ramp(x, y)) return std::nullopt;

	auto const g = ramps_(x, y).back().type;
	int const h = map_.height(x, y);

	return surface(g, x, y, h + (ramps_(x, y).size() - 1) + (std::get_if<flat>(&g) ? 1 : 0));
}

std::optional<int> ramps::effective_height(geom::point<int, 2> const & tile) const
{
	if (tile[0] < 0 || tile[0] >= ramps_.dim(0) || tile[1] < 0 || tile[1] >= ramps_.dim(1)) return std::nullopt;
	if (ramps_(tile[0], tile[1]).empty())
		return map_.height(tile[0], tile[1]);
	auto const g = ramps_(tile[0], tile[1]).back().type;
	return map_.height(tile[0], tile[1]) + (ramps_(tile[0], tile[1]).size() - 1) + (std::get_if<flat>(&g) ? 1 : 0);
}

std::optional<tile> ramps::effective_geometry(geom::point<int, 2> const & tile) const
{
	if (tile[0] < 0 || tile[0] >= ramps_.dim(0) || tile[1] < 0 || tile[1] >= ramps_.dim(1)) return std::nullopt;
	if (!ramps_(tile[0], tile[1]).empty())
		return ramps_(tile[0], tile[1]).back().type;
	return map_.geometry(tile[0], tile[1]);
}

std::optional<std::array<geom::point<float, 3>, 4>> ramps::effective_surface(geom::point<int, 2> const & tile) const
{
	if (has_ramp(tile[0], tile[1]))
		return ramp_surface(tile[0], tile[1]);
	return map_.tile_surface(tile[0], tile[1]);
}

std::optional<float> ramps::effective_center_height(geom::point<int, 2> const & tile) const
{
	if (has_ramp(tile[0], tile[1]))
	{
		auto g = ramps_(tile[0], tile[1]).back().type;
		if (std::get_if<flat>(&g))
			return map_.height(tile[0], tile[1]) + ramps_(tile[0], tile[1]).size();
		return map_.height(tile[0], tile[1]) + ramps_(tile[0], tile[1]).size() - 0.5f;
	}
	return map_.tile_center_height(tile[0], tile[1]);
}

float ramps::height_at(float x, float y) const
{
	int cx = std::floor(x);
	int cy = std::floor(y);

	if (cx < 0 || cx >= ramps_.dim(0) || cy < 0 || cy >= ramps_.dim(1)) return 0.f;

	float tx = x - cx;
	float ty = y - cy;

	int h = *effective_height({cx, cy});
	auto g = *effective_geometry({cx, cy});

	if (std::get_if<flat>(&g))
		return h;

	auto s = std::get_if<slope>(&g);

	if (s->direction == 0)
		return h + tx;
	else if (s->direction == 1)
		return h + ty;
	else if (s->direction == 2)
		return h + 1.f - tx;
	else
		return h + 1.f - ty;
}

geom::vector<float, 3> ramps::normal_at(float x, float y) const
{
	int cx = std::floor(x);
	int cy = std::floor(y);

	if (cx < 0 || cx >= ramps_.dim(0) || cy < 0 || cy >= ramps_.dim(1)) return {0.f, 0.f, 1.f};

	auto g = *effective_geometry({cx, cy});

	if (std::get_if<flat>(&g))
		return {0.f, 0.f, 1.f};

	auto s = std::get_if<slope>(&g);

	if (s->direction == 0)
		return geom::normalized(geom::vector{-1.f, 0.f, 1.f});
	else if (s->direction == 1)
		return geom::normalized(geom::vector{0.f, -1.f, 1.f});
	else if (s->direction == 2)
		return geom::normalized(geom::vector{1.f, 0.f, 1.f});
	else
		return geom::normalized(geom::vector{0.f, 1.f, 1.f});
}

void ramps::update(float dt)
{
	appearing_ramps_ = 0;
	for (int y = 0; y < ramps_.dim(1); ++y)
	{
		for (int x = 0; x < ramps_.dim(0); ++x)
		{
			for (auto & r : ramps_(x, y))
			{
				r.appearing = std::min(1.f, r.appearing + 2.f * dt);

				if (r.appearing < 1.f)
					++appearing_ramps_;
			}
		}
	}
}

bool ramps::has_animation() const
{
	return appearing_ramps_ > 0;
}

void ramps::render(render_options const & options)
{
	program_.bind();
	program_["u_camera_transform"] = options.camera_transform;
	program_["u_light_dir"] = options.light_dir;
	program_["u_color"] = geom::vector{0.4f, 0.2f, 0.f, 1.f};

	flat_mesh_.bind();
	for (int y = 0; y < ramps_.dim(1); ++y)
	{
		for (int x = 0; x < ramps_.dim(0); ++x)
		{
			for (auto & r : ramps_(x, y))
			{
				if (!std::get_if<flat>(&r.type)) continue;

				float rot = r.rotation;

				program_["u_position"] = r.position - geom::vector{0.f, 0.f, 1.f - r.appearing};
				program_["u_rotation"] = geom::vector{std::cos(rot), std::sin(rot)};
				gl::DrawArrays(gl::TRIANGLES, 0, flat_mesh_.vertex_count());
			}
		}
	}

	slope_mesh_.bind();
	for (int y = 0; y < ramps_.dim(1); ++y)
	{
		for (int x = 0; x < ramps_.dim(0); ++x)
		{
			for (auto & r : ramps_(x, y))
			{
				if (!std::get_if<slope>(&r.type)) continue;

				float rot = r.rotation;

				program_["u_position"] = r.position - geom::vector{0.f, 0.f, 1.f - r.appearing};
				program_["u_rotation"] = geom::vector{std::cos(rot), std::sin(rot)};
				gl::DrawArrays(gl::TRIANGLES, 0, slope_mesh_.vertex_count());
			}
		}
	}
}

void ramps::render_ghost(render_options const & options, geom::point<int, 2> const & pos, tile type, gfx::color_4f const & color)
{
	program_.bind();
	program_["u_camera_transform"] = options.camera_transform;
	program_["u_light_dir"] = options.light_dir;
	program_["u_color"] = color;
	program_["u_position"] = geom::vector{pos[0] + 0.5f, pos[1] + 0.5f, (effective_height(pos).value_or(0.f)) * 1.f};

	if (std::get_if<flat>(&type))
	{
		flat_mesh_.bind();
		program_["u_rotation"] = geom::vector{1.f, 0.f};
		gl::DrawArrays(gl::TRIANGLES, 0, flat_mesh_.vertex_count());
	}
	else if (auto s = std::get_if<slope>(&type))
	{
		slope_mesh_.bind();
		float a = s->direction * geom::pi / 2.f;
		program_["u_rotation"] = geom::vector{std::cos(a), std::sin(a)};
		gl::DrawArrays(gl::TRIANGLES, 0, slope_mesh_.vertex_count());
	}
}
